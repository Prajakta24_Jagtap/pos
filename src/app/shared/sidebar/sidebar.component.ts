import { Component, OnInit } from '@angular/core';
import { access } from '../Response.model';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  accessModel=new access();

  constructor() { }

  ngOnInit() {
    this.accessModel.category=eval(sessionStorage.getItem('category'));
    this.accessModel.subcategory=eval(sessionStorage.getItem('subcategory'));
    this.accessModel.brand=eval(sessionStorage.getItem('brand'));
    this.accessModel.unit=eval(sessionStorage.getItem('unit'));
    this.accessModel.product=eval(sessionStorage.getItem('product'));
    this.accessModel.employee=eval(sessionStorage.getItem('employee'));
    this.accessModel.currentstock=eval(sessionStorage.getItem('currentstock'));
    this.accessModel.saleform=eval(sessionStorage.getItem('saleform'));
    this.accessModel.stock=eval(sessionStorage.getItem('stock'));
    this.accessModel.stockhistory=eval(sessionStorage.getItem('stockhistory'));
    this.accessModel.invoicenowisedetails=eval(sessionStorage.getItem('invoicenowisedetails'));
    this.accessModel.adminsetting=eval(sessionStorage.getItem('adminsetting'));
    this.accessModel.changepwd=eval(sessionStorage.getItem('changepwd'));


  }

}
