import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { productMaster } from 'src/app/master/masterModels';
import { getStockAlert } from 'src/app/report/reportUrls';
import { HttpmethodsService } from 'src/app/service/httpmethods.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Output() toggleSideBarForMe:EventEmitter<any> = new EventEmitter();
  cnt: any;
  fullName: any;
  
  constructor(private formBuilder: FormBuilder, public toastr: ToastrManager, private router: Router, private httpService: HttpmethodsService,) { }

ngOnInit() {

  this.getStockAlert();
  this.fullName=sessionStorage.getItem('fullName');
}
toggleSideBar(){
this.toggleSideBarForMe.emit();
}
getStockAlert() {
  // this.httpService.getRequest(getStockAlert).subscribe((data: any) => {
  // console.log(data);
  // // alert(JSON.stringify(data));
  // this.dataSource = new MatTableDataSource(data);
  // this.dataSource.paginator = this.paginator;
  // //ng serve alert(JSON.stringify(this.dataSource));
  // })
  
  this.httpService.getRequest(getStockAlert).subscribe((data: any) => {
  console.log(data);
  this.cnt=data.length;
  //alert("length:"+JSON.stringify(data.length))
  // alert(JSON.stringify(data));

  //ng serve alert(JSON.stringify(this.dataSource));
  
  
  })
}
}