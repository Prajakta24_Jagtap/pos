export class ResponseBase<T>{
    IsSuccess: boolean;
    Message: string;
    Data: T;
    IsNext: boolean;
    TotalRecords: number;
}

export class access{
    category:boolean;
    subcategory:boolean;
    product:boolean;
    brand:boolean;
    unit:boolean;
    employee:boolean;
    currentstock:boolean;
    saleform:boolean;
    stock:boolean;
    stockhistory:boolean;
    invoicenowisedetails:boolean;
    adminsetting:boolean;
    changepwd:boolean;
}