import { Component, OnInit } from '@angular/core';
import { loginMaster } from 'src/app/master/masterModels';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ToastrManager } from 'ng6-toastr-notifications';
import { Router } from '@angular/router';
import { HttpmethodsService } from 'src/app/service/httpmethods.service';
import { login } from 'src/app/master/mastersUrls';

@Component({
  selector: 'app-loginpage',
  templateUrl: './loginpage.component.html',
  styleUrls: ['./loginpage.component.css']
})
export class LoginpageComponent implements OnInit {

  // hide: boolean = true;
  emploginModel= new loginMaster ;
  loginForm: FormGroup;
  fieldTextType : boolean;
  showSpinner : boolean = false;
  constructor(public toastr: ToastrManager, private formBuilder: FormBuilder,private router: Router, private httpService: HttpmethodsService,) { }


  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      
      mobileNumber: new FormControl('',[Validators.required,Validators.pattern("[0-9\g]*$"), Validators.maxLength(15), Validators.minLength(10)]),
      empPassword:new FormControl('', Validators.required)
    })
    
    // this.Login();
  }

  Login() {
    // alert("Login click")
   

    
    console.log();
    // this.adminLoginModel.superAdminMobile="814972g7606";
    // this.adminLoginModel.superAdminPassword="12345678";
    // console.log(JSON.stringify(this.adminLoginModel));
//  `   alert("Blank")`
this.showSpinner = true;
    this.httpService.postRequest(login, this.emploginModel).subscribe((data : any) => {
      // console.log(data);

this.showSpinner = false;

      if (data.responseCode== 200) { 
       // alert("Success");
        this.toastr.successToastr(data.message,'Success!');

        sessionStorage.setItem("employeeId",data.employeeId);
        sessionStorage.setItem("fullName",data.fullName);
        sessionStorage.setItem("empMailAddress",data.empMailAddress);
        sessionStorage.setItem("empAddress",data.empAddress);
        sessionStorage.setItem("employeeDOB",data.employeeDOB);

        sessionStorage.setItem("category",data.category);
        sessionStorage.setItem("subcategory",data.subcategory);
        sessionStorage.setItem("product",data.product);
        sessionStorage.setItem("brand",data.brand);
        sessionStorage.setItem("unit",data.unit);
        sessionStorage.setItem("employee",data.employee);
        sessionStorage.setItem("currentstock",data.currentstock);
        sessionStorage.setItem("saleform",data.saleform);
        sessionStorage.setItem("stock",data.stock);
        sessionStorage.setItem("stockhistory",data.stockhistory);
        sessionStorage.setItem("invoicenowisedetails",data.invoicenowisedetails);
        sessionStorage.setItem("adminsetting",data.adminsetting);
        sessionStorage.setItem("changepwd",data.changepwd);

 
        this.router.navigateByUrl("/layout")
this.showSpinner = false;

      }
      if (data.responseCode == 404) { 
        // this.toastr.errorToastr(data.message,'Error!');
        this.toastr.errorToastr("Enter Valid Mobile Number")
        this.router.navigateByUrl("/login/loginpage")
        this.showSpinner = false;


      }
      if (data.responseCode == 403) { 
        this.toastr.errorToastr(data.message,'Error!');
        this.router.navigateByUrl("/login/loginpage")
        this.showSpinner = false;

      }
      if (data.responseCode == 400) { 
        this.toastr.errorToastr(data.message,'Error!');
        this.router.navigateByUrl("/login/loginpage")
        this.showSpinner = false;

      }
      

    })

  }

    
  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  } 


  redirectHome(){
    this.router.navigateByUrl("/layout")
  }

}
