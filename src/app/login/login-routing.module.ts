import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ForgotpswComponent } from './forgotpsw/forgotpsw.component';
import { LoginpageComponent } from './loginpage/loginpage.component';


const routes: Routes = [
  {path:'loginpage',component:LoginpageComponent},
  {path:'forgotpsw',component:ForgotpswComponent},


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
