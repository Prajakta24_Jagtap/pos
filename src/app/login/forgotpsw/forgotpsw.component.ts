import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { forgotMaster } from 'src/app/master/masterModels';
import { ToastrManager } from 'ng6-toastr-notifications';
import { Router } from '@angular/router';
import { HttpmethodsService } from 'src/app/service/httpmethods.service';
import { generateOtp, validateOtp, employeeForgotPassword } from 'src/app/master/mastersUrls';

@Component({
  selector: 'app-forgotpsw',
  templateUrl: './forgotpsw.component.html',
  styleUrls: ['./forgotpsw.component.css']
})
export class ForgotpswComponent implements OnInit {
  hide : boolean = true;
  mobileNumber:any;
  showdivotp:boolean=false;
  changeBtn:boolean=false;
  div1:boolean=false;

  forgotForm : FormGroup;
  forgotModel= new forgotMaster();
  generatedOtp: any;
    changeButton : boolean = false;
  

  constructor(public toastr: ToastrManager, private formBuilder: FormBuilder,private router: Router, private httpService: HttpmethodsService,) { }


  ngOnInit() {
    this.forgotForm = this.formBuilder.group({
      
      mobileNumber: new FormControl('',[Validators.required,Validators.pattern("[0-9\g]*$"), Validators.maxLength(15), Validators.minLength(10)]),
      otp:new FormControl('', Validators.required),
      empPassword:new FormControl('', Validators.required),
      newPassword:new FormControl('', Validators.required),

    })


  }

 sendOTP() {
    this.httpService.getRequest(generateOtp + "/" + this.forgotModel.mobileNumber).subscribe((data: any) => {
      if(data.status)
      {
      this.toastr.successToastr('OTP Sent Successfully...!', 'Success!', {timeout : 500});
      this.showdivotp=true;
      this.forgotModel.employeeId=data.employeeId;
      }
      else{
        this.toastr.errorToastr("Please Enter Valid Number,...", 'Error!', {timeout : 500});
      this.showdivotp=false;
      }
    })
  }

  validateOpt() {

   this.httpService.postRequest(validateOtp , this.forgotModel).subscribe((data: any) => {
    //alert(JSON.stringify(data))
    if(data.status)
      {                                              
      this.toastr.successToastr('OTP Validate Successfully...!', 'Success!', {timeout : 500});
      this.div1=true;
      
      }
      else{
        this.toastr.errorToastr("Please Enter Valid OTP,...", 'Error!', {timeout : 500});
      this.div1=false;
      }
   })
  }                 

  changePsw()
  {
    this.httpService.postRequest(employeeForgotPassword, this.forgotModel).subscribe((data: any) => {
      if(data.status){
      this.forgotForm.reset();
      this.toastr.successToastr('Password Update Successfully...!', 'Success!', {timeout : 500});
      this.router.navigateByUrl("/login/loginpage")
      // this.div1=false;
      // this.showdivotp=false;
      // this.changeBtn=false;
      }
      else{
        this.toastr.successToastr('{Password Not Update Successfully...!', 'Success!', {timeout : 500});

      }
    })
  }

  changeBtnFun()
  {
    this.changeBtn=true;
  }

  changeButon(event){
// alert(event.target.value)
this.changeButton = true;
//alert(this.changeButton)
  }
}
