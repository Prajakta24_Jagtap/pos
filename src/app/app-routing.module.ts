import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  { path: '', pathMatch: "full", redirectTo: 'home'},
  {path:'home',component:HomeComponent},
  { path: 'auth', loadChildren: () => import('./auth/auth.module').then(mod => mod.AuthModule) },
  { path: 'login', loadChildren: () => import('./login/login.module').then(mod => mod.LoginModule) },
 

   
  {
    path: 'layout', component: LayoutComponent,
    children: [
      {path: '', component:DashboardComponent},
      // { path: '', loadChildren: () => import('./booksize/booksize.module').then(mod=> mod.BooksizeModule)},      // { path: '', loadChildren: () => import('./admin/admin.module').then(mod=> mod.AdminModule)},
      { path: '', loadChildren: () => import('./master/master.module').then(mod => mod.MasterModule) },
      { path: '', loadChildren: () => import('./report/report.module').then(mod => mod.ReportModule) },
     
      ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
