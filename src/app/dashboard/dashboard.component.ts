import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator,MatDialog, MatSort, MatTableDataSource } from '@angular/material';
import Chart from 'chart.js';
import { adminsetting, productMaster } from '../master/masterModels';
import { getAllProduct, getAllsettingCount } from '../master/mastersUrls';
import { HttpmethodsService } from 'src/app/service/httpmethods.service';
import { getDashboardTotalFinnalAmount } from '../report/reportUrls';
import { dashboardfinalamount } from '../report/reportModels';
import { formatDate } from '@angular/common';
// import * as $ from 'jquery' ;
@Component({
selector: 'app-dashboard',
templateUrl: './dashboard.component.html',
styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

@ViewChild(MatSort, {static: true}) sort: MatSort;
@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

productStockModel= new productMaster();
adminsetting=new adminsetting();
obj=new dashboardfinalamount();


constructor(public dialog: MatDialog, private httpService: HttpmethodsService) { }
displayedColumns: string[] = ['no','categoryname','subcategoryname'];
dataSource = new MatTableDataSource<productMaster>();
displayedColumns1: string[] = ['no','lowStock'];
dataSource1 = new MatTableDataSource<adminsetting>();





public canvas : any;
public ctx;
public chartColor;
public chartEmail;
public chartHours;

ngOnInit(){

this.dataSource.sort = this.sort;

this.obj.dailtDate=formatDate(new Date, 'yyyy-MM-dd', 'en');
this.obj.weekDate=formatDate(new Date, 'yyyy-MM-dd', 'en');
this.obj.yaerDate=formatDate(new Date, 'yyyy-MM-dd', 'en');
this.obj.monthDate=formatDate(new Date, 'yyyy-MM-dd', 'en');

this.getFinalAmount();

this.getAllProduct();
this. getAllStock();
// this.chartColor = "#FFFFFF";

// this.canvas = document.getElementById("chartHours");
// this.ctx = this.canvas.getContext("2d");

// this.chartHours = new Chart(this.ctx, {
// type: 'line',

// data: {
// labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct"],
// datasets: [{
// borderColor: "#6bd098",
// backgroundColor: "#6bd098",
// pointRadius: 0,
// pointHoverRadius: 0,
// borderWidth: 3,
// data: [300, 310, 316, 322, 330, 326, 333, 345, 338, 354]
// },
// {
// borderColor: "#f17e5d",
// backgroundColor: "#f17e5d",
// pointRadius: 0,
// pointHoverRadius: 0,
// borderWidth: 3,
// data: [320, 340, 365, 360, 370, 385, 390, 384, 408, 420]
// },
// {
// borderColor: "#fcc468",
// backgroundColor: "#fcc468",
// pointRadius: 0,
// pointHoverRadius: 0,
// borderWidth: 3,
// data: [370, 394, 415, 409, 425, 445, 460, 450, 478, 484]
// }
// ]
// },
// options: {
// legend: {
// display: false
// },

// tooltips: {
// enabled: false
// },

// scales: {
// yAxes: [{

// ticks: {
// fontColor: "#9f9f9f",
// beginAtZero: false,
// maxTicksLimit: 5,
// //padding: 20
// },
// gridLines: {
// drawBorder: false,
// zeroLineColor: "#ccc",
// color: 'rgba(255,255,255,0.05)'
// }

// }],

// xAxes: [{
// barPercentage: 1.6,
// gridLines: {
// drawBorder: false,
// color: 'rgba(255,255,255,0.1)',
// zeroLineColor: "transparent",
// display: false,
// },
// ticks: {
// padding: 20,
// fontColor: "#9f9f9f"
// }
// }]
// },
// }
// });


// this.canvas = document.getElementById("chartEmail");
// this.ctx = this.canvas.getContext("2d");
// this.chartEmail = new Chart(this.ctx, {
// type: 'pie',
// data: {
// labels: [1, 2, 3],
// datasets: [{
// label: "Emails",
// pointRadius: 0,
// pointHoverRadius: 0,
// backgroundColor: [
// '#e3e3e3',
// '#4acccd',
// '#fcc468',
// '#ef8157'
// ],
// borderWidth: 0,
// data: [342, 480, 530, 120]
// }]
// },

// options: {

// legend: {
// display: false
// },

// pieceLabel: {
// render: 'percentage',
// fontColor: ['white'],
// precision: 2
// },

// tooltips: {
// enabled: false
// },

// scales: {
// yAxes: [{

// ticks: {
// display: false
// },
// gridLines: {
// drawBorder: false,
// zeroLineColor: "transparent",
// color: 'rgba(255,255,255,0.05)'
// }

// }],

// xAxes: [{
// barPercentage: 1.6,
// gridLines: {
// drawBorder: false,
// color: 'rgba(255,255,255,0.1)',
// zeroLineColor: "transparent"
// },
// ticks: {
// display: false,
// }
// }]
// },
// }
// });

// var speedCanvas = document.getElementById("speedChart");

// var dataFirst = {
// data: [0, 19, 15, 20, 30, 40, 40, 50, 25, 30, 50, 70],
// fill: false,
// borderColor: '#fbc658',
// backgroundColor: 'transparent',
// pointBorderColor: '#fbc658',
// pointRadius: 4,
// pointHoverRadius: 4,
// pointBorderWidth: 8,
// };

// var dataSecond = {
// data: [0, 5, 10, 12, 20, 27, 30, 34, 42, 45, 55, 63],
// fill: false,
// borderColor: '#51CACF',
// backgroundColor: 'transparent',
// pointBorderColor: '#51CACF',
// pointRadius: 4,
// pointHoverRadius: 4,
// pointBorderWidth: 8
// };

// var speedData = {
// labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
// datasets: [dataFirst, dataSecond]
// };

// var chartOptions = {
// legend: {
// display: false,
// position: 'top'
// }
// };

// var lineChart = new Chart(speedCanvas, {
// type: 'line',
// hover: false,
// data: speedData,
// options: chartOptions
// });
}


getAllProduct() {
this.httpService.getRequest(getAllProduct).subscribe((data: any) => {
// console.log(data);
this.dataSource = new MatTableDataSource(data);
this.dataSource.paginator = this.paginator;
// alert(JSON.stringify(this.dataSource));
})
}
getAllStock() {
this.httpService.getRequest(getAllsettingCount).subscribe((data: any) => {
// console.log(data);
// alert(JSON.stringify(data));
this.dataSource1 = new MatTableDataSource(data);
this.dataSource1.paginator = this.paginator;
//ng serve alert(JSON.stringify(this.dataSource));
})
}



getFinalAmount()
{
this.httpService.postRequest(getDashboardTotalFinnalAmount, this.obj).subscribe((data: any) => {
// console.log(data);


// alert("Profit --- "+data);

this.obj.daily="Daily Total";
this.obj.monthly="Monthly Total";
this.obj.yearly="Yearly Total";
this.obj.weekly="Weekly Total";

if(data.totalFinalCountDaily==null){
    this.obj.totalFinalCountDaily=0;
}
else{
this.obj.totalFinalCountDaily=data.totalFinalCountDaily;
}

if(data.totalFinalCountWeekly==null){
    this.obj.totalFinalCountWeekly=0;
}
else{
this.obj.totalFinalCountWeekly=data.totalFinalCountWeekly;
}

if(data.totalFinalCountMonthly==null){
    this.obj.totalFinalCountMonthly=0;
}
else{
this.obj.totalFinalCountMonthly=data.totalFinalCountMonthly;
}

if(data.totalFinalCountYearly==null){
    this.obj.totalFinalCountYearly=0;
}
else{
this.obj.totalFinalCountYearly=data.totalFinalCountYearly;
}

})

}

}