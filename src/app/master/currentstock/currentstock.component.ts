import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ToastrManager } from 'ng6-toastr-notifications';
import { HttpmethodsService } from 'src/app/service/httpmethods.service';
import { productMaster } from '../masterModels';
import { getAllProduct } from '../mastersUrls';
import * as xlsx from 'xlsx';

@Component({
  selector: 'app-currentstock',
  templateUrl: './currentstock.component.html',
  styleUrls: ['./currentstock.component.css']
})
export class CurrentstockComponent implements OnInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild('epltable', { static: false }) epltable: ElementRef;

  productStockForm: FormGroup;
  productStockModel= new productMaster();

 constructor(private formBuilder: FormBuilder,public toastr: ToastrManager, private httpService: HttpmethodsService) { }
  //displayedColumns: string[] = ['no', 'countryname', 'statename', 'status','Action'];
  displayedColumns: string[] = ['no','categoryname','subcategoryname'];

  dataSource = new MatTableDataSource<productMaster>();


  
  ngOnInit() {
    
    this.dataSource.sort = this.sort;
    this.getAllProduct();
  }

  getAllProduct() {
    this.httpService.getRequest(getAllProduct).subscribe((data: any) => {
     // console.log(data);
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
     // alert(JSON.stringify(this.dataSource));
    })
  }

  exportToExcel() {
    const ws: xlsx.WorkSheet =
    xlsx.utils.table_to_sheet(this.epltable.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'Product Current Stock Reports.xlsx');
    }

    
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
