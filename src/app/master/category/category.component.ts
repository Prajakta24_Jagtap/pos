import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { categoryMaster, nameexistmaster } from '../masterModels';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ToastrManager } from 'ng6-toastr-notifications';
import { Router } from '@angular/router';
import { HttpmethodsService } from 'src/app/service/httpmethods.service';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { createCategory, updateCategory, getAllCategory, getCategoryById, checkAlreadyExistName } from '../mastersUrls';
import * as xlsx from 'xlsx';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  categoryModel=new categoryMaster();
  categoryForm: FormGroup;
  isEdit: boolean = true;
  nameexistmaster=new nameexistmaster()

  
  constructor(private formBuilder: FormBuilder, public toastr: ToastrManager, private router: Router, private httpService: HttpmethodsService,) { }
  displayedColumns: string[] = ['no', 'countryname','status', 'Action'];
  dataSource = new MatTableDataSource<categoryMaster>();
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild('epltable', { static: false }) epltable: ElementRef;

  ngOnInit() {

    this.categoryForm = this.formBuilder.group({
      //   categoryName: new FormControl('', [Validators.required, Validators.pattern("[a-zA-Z\g]* $"),]),
        categoryName: new FormControl('', [Validators.required]),
        categoryStatus: new FormControl('', [Validators.required])
        
   
       })

       this.getAllCategory();     
  }

  addCategory(){
    if (this.isEdit) {
      this.nameexistmaster.categoryName=this.categoryModel.categoryName;
      this.httpService.postRequest(checkAlreadyExistName,this.nameexistmaster).subscribe((data: any) => {
        if(data)
        {
         
       this.httpService.postRequest(createCategory, this.categoryModel).subscribe((data: any) => {
          if (data) {
            this.toastr.successToastr("Save Successfully...", 'Success!');
            this.isEdit = true;
           this.getAllCategory();
            this.categoryForm.reset();
          }
          else {
            this.toastr.errorToastr("Save Faield...", 'Error!');
           this.getAllCategory();
  
            this.isEdit = true;
          }
        })
        }
       else
       {
        this.toastr.errorToastr("Already Exist Category Name ", 'Error!');
       }
        // if (data)
        //  {
      
        // this.httpService.postRequest(createCategory, this.categoryModel).subscribe((data: any) => {
        //   if (data) {
        //     this.toastr.successToastr("Save Successfully...", 'Success!');
        //     this.isEdit = true;
        //    this.getAllCategory();
        //     this.categoryForm.reset();
        //   }
        //   else {
        //     this.toastr.errorToastr("Save Faield...", 'Error!');
        //    this.getAllCategory();
  
        //     this.isEdit = true;
        //   }
        // })
        // }
       
      })
        
      // alert("Add")
      
     }
     else {
      // alert("update");
       this.httpService.putRequest(updateCategory, this.categoryModel).subscribe((data: any) => {
         if (data) {
           this.toastr.successToastr("Update Successfully...", 'Success!');
           this.isEdit = true;
          this.getAllCategory();
           this.categoryForm.reset();
         }
         else {
           this.toastr.errorToastr("Update Faield...", 'Error!');
          this.getAllCategory();
           this.isEdit = true;
         }
       })
     }
 
  } 

  getAllCategory() {
    this.httpService.getRequest(getAllCategory).subscribe((data: any) => {
   //   console.log(data);
    //  alert(JSON.stringify(data));
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
      //ng serve alert(JSON.stringify(this.dataSource));
    })
  }

  getCategoryById(element) {
 //   this.dataService.dataObj=element;
  //  alert(JSON.stringify(this.dataService.dataObj))
    //alert(JSON.stringify(element.categoryId));
    this.httpService.getRequest(getCategoryById + "/" + element.categoryId).subscribe((data: any) => {
    //  console.log(data);
      this.categoryModel = data;
      this.isEdit = false;

    })

  }

  exportToExcel() {
    const ws: xlsx.WorkSheet =
    xlsx.utils.table_to_sheet(this.epltable.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'Category Reports.xlsx');
    }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}

