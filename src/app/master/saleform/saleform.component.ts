import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { saleMaster, prodMaster } from '../masterModels';
import { ToastrManager } from 'ng6-toastr-notifications';
import { HttpmethodsService } from 'src/app/service/httpmethods.service';
import { getRetailList, getNameByMobNo, getWholesaleList, createOrder, getInvoiceNo, invoiceNoWiseOrder, getByBarcodeData, getByBarcodeDataRetail } from '../mastersUrls';
import { formatDate } from '@angular/common';
import * as xlsx from 'xlsx';

@Component({
  selector: 'app-saleform',
  templateUrl: './saleform.component.html',
  styleUrls: ['./saleform.component.css']
})
export class SaleformComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild('epltable', { static: false }) epltable: ElementRef;

  //For change focus
  @ViewChild("qty",{static:true}) qtyField: ElementRef;
  qty: number;
  showLooseUnit: boolean=false;
  @ViewChild("barCode",{static:true}) barCodeField: ElementRef;
  barCode: string;
 // showSpinner : boolean = false;
  buttonDisabled: boolean = false;
 
 

  editQty():void{
  
    this.qtyField.nativeElement.focus();
    this.row.qty=1;

    // alert(Math.round(Number(this.row.qty) * Number(this.row.rate)))
    this.row.finalAmount=Math.round(Number(this.row.qty) * Number(this.row.rate));
  }

  @ViewChild("productFocus",{static:true}) productNameField:ElementRef;
  setFocusToProductName():void{
    this.productNameField.nativeElement.focus();
  }

  editBarcode():void{ 
    this.barCodeField.nativeElement.focus();
  
   // this.row.finalAmount=Math.round(Number(this.row.qty) * Number(this.row.rate));
  }

  // @ViewChild("barCodeFocus",{static:true}) barCodeField:ElementRef;
  // setFocusToBarCode():void{
  //   this.barCodeField.nativeElement.focus();
  // }

  saleForm: FormGroup;
  saleModel= new saleMaster();
 
  isEdit: boolean = true;
 retailList: any[];
 wholesaleList: any[];
 mobileNoList:any[];
 row= new prodMaster();
 productList =new Array<prodMaster>();
 saleModule=new saleMaster();

 // setInvoiceNo: string;
  // datePipe: any;
 // totalQty:number=0;
  //quantityList=new Array<prodMaster>();
 


  constructor(private formBuilder: FormBuilder,public toastr: ToastrManager, private httpService: HttpmethodsService) { }
  //displayedColumns: string[] = ['no', 'countryname', 'statename', 'status','Action'];
  displayedColumns: string[] = ['no','categoryname','unitname','subcategoryname','brandname','discountPerc','discount','rate','purchasePrice', 'countryname','Action'];

  dataSource = new MatTableDataSource<prodMaster>();


  

  ngOnInit() {
    this.saleForm = this.formBuilder.group({
      saleId:new FormControl(''),
      invoiceDate:new FormControl('', [Validators.required]),
      saleType:new FormControl('', [Validators.required]),
      cashier:new FormControl('', [Validators.required]),
      invoiceNo:new FormControl('', [Validators.required]),
    payType:new FormControl('', [Validators.required]),
    custMobileNumber:new FormControl('', [Validators.required]),
    customerName:new FormControl('', [Validators.required]),
    productName:new FormControl(''),
    productDescription:new FormControl('', [Validators.required]),
    qty:new FormControl('', [Validators.required]),
    mrp:new FormControl('', [Validators.required]),
    rate:new FormControl('', [Validators.required]),
    finalAmount:new FormControl('', [Validators.required]),
    unitName:new FormControl('', [Validators.required]),
    discountPerc:new FormControl('', [Validators.required]),
    discount:new FormControl('', [Validators.required]),

    employeeId:new FormControl(''),
    currentStock:new FormControl(''), 
    looseUnit:new FormControl(''),
    unitDropDown:new FormControl(''),

    purchasePrice:new FormControl(''),
    totalPurchasePrice:new FormControl(''),
    barCode:new FormControl('')

    })


    this.dataSource.sort = this.sort;
    // this.dataSource.paginator = this.paginator;
    this.getInvoiceNo();
   
    this.saleModel.invoiceDate = formatDate(new Date(), 'yyyy-MM-dd', 'en');

    this.saleModel.saleType="Retail";

    this.saleModel.payType="Cash";
    this.row.looseUnit=false;
    this.row.unitDropDown="gm";

    this.saleModel.cashier=sessionStorage.getItem('fullName');
   // alert("fname="+JSON.stringify(this.saleModel.cashier))
   this.saleModel.employeeId=parseInt(sessionStorage.getItem('employeeId'));
    
  }

  getInvoiceNo(){
    this.httpService.getRequest(getInvoiceNo).subscribe((data: any) => {
      this.saleModel.invoiceNo = data.invoiceNo; 
      //alert("invoice no="+JSON.stringify(this.saleModel.invoiceNo))
     // this.setInvoiceNo=this.saleModel.invoiceNo;
      })
  }
  
  // getDetailsByInvoiceNo(){
  //   this.httpService.getRequest(invoiceNoWiseOrder+"/"+this.saleModel.invoiceNo).subscribe((data: any) => {
  //     console.log(data)
  //     //alert("invoice no="+JSON.stringify(this.saleModel.invoiceNo))
     

      

  //     })
  // }

  getByIdSearch(data){
   // alert("data="+JSON.stringify(data))
   if(data.looseUnit==true)
    {
          this.showLooseUnit=true;
          this.row.looseUnit=true;
          
    }
    else{
      this.showLooseUnit=false;
      this.row.looseUnit=false;
    }
    this.row = data;
    //console.log(this.row);
    
  }

  flag:boolean=false;
  index:number=0;
  addDetails(row){
//alert(JSON.stringify(row))
    
  //   this.saleModel.productList.push(this.row);
  //   this.dataSource.data=this.saleModel.productList;
  //  // alert(JSON.stringify( this.dataSource.data))
  //   this.row= new prodMaster();
  //   this.saleForm.controls['productName'].reset();
  //   this.calculation();
  if(this.saleModel.customerName==null || this.saleModel.custMobileNumber==null)
  {
    alert("Please Enter Customer Information...")
  }
  if(this.saleModel.productName==null)
  {
    alert("Please Enter Product Information...")
  }
  if(this.saleModel.customerName!=null && this.saleModel.custMobileNumber!=null && this.saleModel.productName!=null)
  {
  if(this.saleModel.productList.length==0){
   
    this.saleModel.productList.push(this.row);
  //  alert("list id="+JSON.stringify(this.row.productId))
 // console.log("listttt"+JSON.stringify(this.saleModel.productList));
    this.dataSource.data=this.saleModel.productList;
    // alert(JSON.stringify( this.dataSource.data))
    this.row= new prodMaster();
    this.saleForm.controls['productName'].reset();
    this.saleForm.controls['barCode'].reset();
    
    this.calculation();
  }
  else{
  //  alert("list size>0")
    //console.log("elseeslistttt"+JSON.stringify(this.saleModel.productList));
    for (let j = 0; j < this.saleModel.productList.length; j++) {
      if(this.saleModel.productList[j].productName==row.productName)
      {
        this.flag=true;
        this.index=j;
        break;
      }
    }

    if(this.flag==true)
    {
      this.saleModel.productList[this.index].qty= Number(this.row.qty)+Number(this.saleModel.productList[this.index].qty);
      this.saleModel.productList[this.index].mrp = this.row.mrp + this.saleModel.productList[this.index].mrp;
      this.saleModel.totalItems=this.saleModel.totalItems+1;
      this.saleModel.productList[this.index].rate=this.row.rate+this.saleModel.productList[this.index].rate;
      this.saleModel.productList[this.index].purchasePrice=this.row.purchasePrice+this.saleModel.productList[this.index].purchasePrice;
      this.saleModel.productList[this.index].finalAmount=this.row.finalAmount+this.saleModel.productList[this.index].finalAmount;

      this.row= new prodMaster();
      this.saleForm.controls['productName'].reset();
      this.saleForm.controls['barCode'].reset();
   
      this.calculation();
      this.flag=false;
    }
    else
    {
      this.saleModel.productList.push(this.row);
      this.dataSource.data=this.saleModel.productList;
      this.row= new prodMaster();
      this.saleForm.controls['productName'].reset();
      this.saleForm.controls['barCode'].reset();
   
      this.calculation();
    }


  }


  }
 
 
   }
  
   editDetails(element,i)
   {
     //alert(JSON.stringify(element))
     this.row= element;
     this.saleModel.productList.splice(i,1);
     this.calculation();
     this.dataSource.data= this.saleModel.productList;
   }

   removeData(index)
  {
    this.saleModel.productList.splice(index,1);
    this.calculation();
    this.dataSource.data=this.saleModel.productList;
  }

  

    getProdList(event) 
    {
      this.saleModel.productName = event.target.value;
      // alert(this.saleModel.productName)
      if(this.saleModel.saleType=="Retail"){
      this.httpService.getRequest(getRetailList+"/"+this.saleModel.productName).subscribe((data: any) => {
        // console.log(data);
        // this.saleModel = data;
         this.retailList=data;
         
        // this.saleModel.orderDetailReqDtoList=this.retailList;
        // alert(JSON.stringify( this.retailList))
         })
        }
        else if(this.saleModel.saleType=="Wholesale"){
          this.httpService.getRequest(getWholesaleList+"/"+this.saleModel.productName).subscribe((data: any) => {
                  // console.log(data);
                //   this.saleModel = data;
                   this.retailList=data;
                  // this.saleModel.orderDetailReqDtoList=this.retailList;
                   //alert(JSON.stringify( this.retailList))
                   })
           }
     
      
    }

    // getByBarcode()
    // {
    //  //alert(JSON.stringify(this.saleModel));
    //   this.httpService.getRequest(getByBarcodeData +"/"+this.saleModel.barCode).subscribe((data: any) => {
    // // this.httpService.getRequest(getByBarcodeData+"/"+this.saleModel.barCode).subscribe((data: any) => {
    //   // console.log(data);
    // //   this.saleModel = data;
    //    this.row=data;
    //    this.row.qty=1;
    //    this.row.finalAmount=this.row.qty*this.row.rate;
    //   alert(JSON.stringify(this.row));
    
    //    })      
    //   }

      getByBarcode()
    {
     //alert(JSON.stringify(this.saleModel));

     if(this.saleModel.saleType=="Retail"){
       //alert("Retail Data");
      this.httpService.getRequest(getByBarcodeDataRetail +"/"+this.saleModel.barCode).subscribe((data: any) => {
      this.row=data;
      this.row.qty=1;
      this.row.finalAmount=this.row.qty*this.row.rate;
       //alert(JSON.stringify(this.row));
             
       })
      }
      else if(this.saleModel.saleType=="Wholesale")
      {
       // alert("Wholesale Data");
        this.httpService.getRequest(getByBarcodeData +"/"+this.saleModel.barCode).subscribe((data: any) => {
          this.row=data;
          this.row.qty=1;
          this.row.finalAmount=this.row.qty*this.row.rate;
          // alert(JSON.stringify(this.row));
           
           })
      }
     }




    applyFilter(event){
      const filterValue = (event.target as HTMLInputElement).value;
      this.dataSource.filter = filterValue.trim().toLowerCase();
  
      if (this.dataSource.paginator) {
        this.dataSource.paginator.firstPage();
      }
    }
      
    calFinalAmt(){

      if(this.row.looseUnit==false)
      {
        alert("Loose Unit false");
        if(this.row.qty>this.row.currentStock)
        {
          // alert("Quantity is greater than current stock")
          alert('Quantity is greater than current stock!');
        }
      }
    

      if(this.row.looseUnit){
      // alert("loose");
      
      if(this.row.unitDropDown=="gm"){
        // alert("gm");
        this.qty= this.row.qty/1000;
        this.row.finalAmount=Math.round(this.qty*this.row.rate);
       // this.row.finalAmount=this.qty*this.row.rate;
        //  alert("gm"+JSON.stringify(this.row.finalAmount));

        // alert("gm"+JSON.stringify(Math.round(this.qty*this.row.rate)));
      }
      if(this.row.unitDropDown=="KG"){
        this.row.finalAmount=Math.round(this.row.qty*this.row.rate);
      }
      if(this.row.unitDropDown=="ml"){
        this.qty=this.row.qty/1000;
        this.row.finalAmount=Math.round(this.qty*this.row.rate);
      }
      if(this.row.unitDropDown=="L"){
        this.row.finalAmount=Math.round(this.row.qty*this.row.rate);
      }
    }
    else{
      //alert("not loose");
      this.row.finalAmount= Math.round(this.row.qty*this.row.rate);
      
    }
    }


    getNameByMobNo(mobNo){
      // alert("getname="+JSON.stringify(mobNo))  
      this.saleModel.customerName='';
      this.saleModel.custMobileNumber=mobNo;
      // alert("getname="+JSON.stringify(this.saleModel.custMobileNumber));
       this.httpService.getRequest(getNameByMobNo+"/"+this.saleModel.custMobileNumber).subscribe((data: any) => {
         // console.log(data);
         this.mobileNoList=data;
          //this.saleModel.customerName = data.customerName;
          
          // alert("mobileNoList="+JSON.stringify( this.mobileNoList))
          })
     }
     setName(data1){
       this.saleModel.customerName=data1.customerName;
     //  alert("customer name="+JSON.stringify( this.saleModel.customerName))
     }

    

    // calculatePro(){
    //   alert("Sachin");  
    // }

    addOrderDetails(){
      // alert("addddd")
    //  this.showSpinner = true;
      if(this.saleModel.custMobileNumber==null || this.saleModel.customerName==null)
      {
      alert("Please fill Customer Information....")
      }
      if(this.saleModel.custMobileNumber!=null && this.saleModel.customerName!=null)
      {
      this.buttonDisabled= true;
      
      // document.getElementById("myBtn").disabled = true;
      this.httpService.postRequestJasper(createOrder, this.saleModel).subscribe((data: any) => {
      // document.getElementById("myBtn").disabled = true;
      //this.showSpinner = false;
      this.buttonDisabled= true;
      if(data){
      //alert("Successfully")
      this.buttonDisabled= false;
      // document.getElementById("myBtn").disabled = false;
      this.toastr.successToastr("Save Successfully...",'Success!');
      
      var file = new Blob([data], { type: 'application/pdf' });
      var fileURL = URL.createObjectURL(file);
      window.open(fileURL);
      //this.showSpinner = false;
      
      }
      else {
      // alert("not works")
      
      this.toastr.errorToastr("Please Try Again....",'Error!')
     // this.showSpinner = false;
      }
      
      // this.toastr.successToastr("Save Successfully...",'Success!');
      
      
      this.saleForm.reset();
      this.saleForm.markAsPristine();
      setTimeout(() => {
      location.reload();
      }, 100);
      
      })
      }
      
      }
      



    calculation(){
      
       
   this.saleModel.totalItems=0;
   this.saleModel.totalQuantity=0;
   this.saleModel.totalMrp=0;
   this.saleModel.totalRate=0;
   this.saleModel.totalPurchasePrice=0;
   this.saleModel.totalFinalAmt=0;
   this.saleModel.youSave=0;
  // this.saleModule.productList
      for (let j = 0; j < this.saleModel.productList.length; j++) {
        //this.totalPrintedAmt =  this.orderProductList[j].totalGST - this.totalPrintedAmt ;

       this.saleModel.totalMrp = this.saleModel.totalMrp + this.saleModel.productList[j].mrp;
       this.saleModel.totalItems=this.saleModel.totalItems+1;
       this.saleModel.totalQuantity= Number(this.saleModel.totalQuantity)+Number(this.saleModel.productList[j].qty);
       this.saleModel.totalRate=this.saleModel.totalRate+this.saleModel.productList[j].rate;
       this.saleModel.totalPurchasePrice=this.saleModel.totalPurchasePrice+this.saleModel.productList[j].purchasePrice;
       this.saleModel.totalFinalAmt=this.saleModel.totalFinalAmt+this.saleModel.productList[j].finalAmount;
      // alert("total Items:"+this.saleModel.totalItems+ " Total Qty:"+ this.saleModel.totalQuantity+"total Mrp:"+this.saleModel.totalMrp+"total price:"+this.saleModel.totalRate+"Total F Amt"+this.saleModel.totalFinalAmt)

     
      }

      this.saleModel.youSave=this.saleModel.totalMrp-this.saleModel.totalRate;
    }

    discountPercentageWise(){
      this.row.discount=this.row.mrp*this.row.discountPerc/100;
      this.row.rate=this.row.mrp-this.row.discount;

      this.calFinalAmt();
    }

    discountAmountWise(){
      this.row.discountPerc=this.row.discount*100/this.row.mrp;
      this.row.rate=this.row.mrp-this.row.discount;

      this.calFinalAmt();
    }

    FinalAmountWise(){
  
      this.row.discount=this.row.mrp-this.row.rate;
      this.row.discountPerc=this.row.discount*100/this.row.mrp;

      this.calFinalAmt();
    }

    exportToExcel() {
      const ws: xlsx.WorkSheet =
      xlsx.utils.table_to_sheet(this.epltable.nativeElement);
      const wb: xlsx.WorkBook = xlsx.utils.book_new();
      xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
      xlsx.writeFile(wb, 'Saleform Reports.xlsx');
      }
    
  
} 