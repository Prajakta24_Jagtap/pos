import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { nameexistmaster, unitMaster } from '../masterModels';
import { ToastrManager } from 'ng6-toastr-notifications';
import { HttpmethodsService } from 'src/app/service/httpmethods.service';
import { getUnitById, getAllUnit, updateUnit, createUnit, checkAlreadyExistName } from '../mastersUrls';
import * as xlsx from 'xlsx';
import { element } from 'protractor';

@Component({
  selector: 'app-unit',
  templateUrl: './unit.component.html',
  styleUrls: ['./unit.component.css']
})
export class UnitComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild('epltable', { static: false }) epltable: ElementRef;
  
  unitForm: FormGroup;
  unitModel= new unitMaster();
  nameexistmaster=new nameexistmaster();
  isEdit: boolean = true;
  categoryActiveList: any[];
  // looseUnit:boolean=false;
  constructor(private formBuilder: FormBuilder,public toastr: ToastrManager, private httpService: HttpmethodsService) { }
  displayedColumns: string[] = ['no', 'countryname', 'status','Action'];
  dataSource = new MatTableDataSource<unitMaster>();


  

  ngOnInit() {
    this.unitForm = this.formBuilder.group({
      unitName: new FormControl('', [Validators.required]),
      looseUnit:new FormControl(''),
      unitStatus:new FormControl('',[Validators.required]),


    })
    this.dataSource.sort = this.sort;
    // this.dataSource.paginator = this.paginator;
  //  this.getActiveCategory();
  this.unitModel.looseUnit=false;
    this.getAllUnit();
  }

  addUnit(){
    if (this.isEdit) {
      // alert("Add")

      this.nameexistmaster.unitName=this.unitModel.unitName;
      this.httpService.postRequest(checkAlreadyExistName,this.nameexistmaster).subscribe((data: any) => {
        if(data)
        {
         

       this.httpService.postRequest(createUnit, this.unitModel).subscribe((data: any) => {
        if (data) {
          this.toastr.successToastr("Save Successfully...", 'Success!');
          this.isEdit = true;
         this.getAllUnit();
          this.unitForm.reset();
        }
        else {
          this.toastr.errorToastr("Save Faield...", 'Error!');
         this.getAllUnit();

          this.isEdit = true;
        }
      })
        }
        else
        {
          this.toastr.errorToastr("Already Exist Unit Name ", 'Error!');
        }
      })
      
     }
     else {
      // alert("update");
       this.httpService.putRequest(updateUnit, this.unitModel).subscribe((data: any) => {
         if (data) {
           this.toastr.successToastr("Update Successfully...", 'Success!');
           this.isEdit = true;
          this.getAllUnit();
           this.unitForm.reset();
         }
         else {
           this.toastr.errorToastr("Update Faield...", 'Error!');
          this.getAllUnit();
           this.isEdit = true;
         }
       })
     }
 
  } 

  getAllUnit() {
    this.httpService.getRequest(getAllUnit).subscribe((data: any) => {
    //  console.log(data);
    //  alert(JSON.stringify(data));
    this.dataSource = new MatTableDataSource(data);
    //this.dataSource.paginator = this.paginator;
      //ng serve alert(JSON.stringify(this.dataSource));
    })
  }

  getUnitById(element) {
 //   this.dataService.dataObj=element;
  //  alert(JSON.stringify(this.dataService.dataObj))
    //alert(JSON.stringify(element.categoryId));
    this.httpService.getRequest(getUnitById + "/" + element.unitId).subscribe((data: any) => {
   //   console.log(data);
      this.unitModel = data;
      this.isEdit = false;

    })
  }

  exportToExcel() {
    const ws: xlsx.WorkSheet =
    xlsx.utils.table_to_sheet(this.epltable.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'Unit Reports.xlsx');
    }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  
  }