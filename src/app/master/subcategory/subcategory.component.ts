import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { nameexistmaster, subCategoryMaster } from '../masterModels';
import { ToastrManager } from 'ng6-toastr-notifications';
import { HttpmethodsService } from 'src/app/service/httpmethods.service';
import { createSubCategory, updateSubCategory, getActiveCategory, getAllSubCategory, getSubCategoryById, checkAlreadyExistName } from '../mastersUrls';
import * as xlsx from 'xlsx';

@Component({
  selector: 'app-subcategory',
  templateUrl: './subcategory.component.html',
  styleUrls: ['./subcategory.component.css']
})
export class SubcategoryComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild('epltable', { static: false }) epltable: ElementRef;
  
  subCategoryForm: FormGroup;
  subCategoryModel= new subCategoryMaster();
  isEdit: boolean = true;
  categoryActiveList: any[];
  nameexistmaster=new nameexistmaster();

  constructor(private formBuilder: FormBuilder,public toastr: ToastrManager, private httpService: HttpmethodsService) { }
  //displayedColumns: string[] = ['no', 'countryname', 'statename', 'status','Action'];
  displayedColumns: string[] = ['no','statename', 'countryname', 'status','Action'];

  dataSource = new MatTableDataSource<subCategoryMaster>();


  

  ngOnInit() {
    this.subCategoryForm = this.formBuilder.group({
      subCategoryName: new FormControl('', [Validators.required]),
      categoryId:new FormControl(''),
      subcategoryStatus:new FormControl('')


    })
    this.dataSource.sort = this.sort;
    // this.dataSource.paginator = this.paginator;
    this.getActiveCategory();
    this.getAllSubCategory();
  }

  getActiveCategory(){
    this.httpService.getRequest(getActiveCategory).subscribe((data: any) => {
     // console.log(data);
      this.categoryActiveList = data;
      })
  }

  addSubCategory(){
    // alert(JSON.stringify(this.subCategoryModel))
     if (this.isEdit) {
      // alert("Add")
      this.nameexistmaster.subcategoryName=this.subCategoryModel.subcategoryName;
      this.httpService.postRequest(checkAlreadyExistName,this.nameexistmaster).subscribe((data: any) => {
        if(data)
        {
          this.httpService.postRequest(createSubCategory, this.subCategoryModel).subscribe((data: any) => {
            if(data){
            this.toastr.successToastr("Save Successfully...",'Success!');
            this.isEdit = true;
           // this.getAllCategory();
          //  setTimeout(() => this.formGroupDirective.resetForm(), 200);
          this.getAllSubCategory();
            this.subCategoryForm.reset(); 
            this.subCategoryForm.markAsPristine();
            }
            else{
              this.toastr.errorToastr("Save Faield...",'Error!');
              //this.getAllCategory();
             this.getAllSubCategory();
              this.isEdit = true;
            }
          })
        }
        else
        {
          this.toastr.errorToastr("Already Exist SubCategory Name ", 'Error!');
        }
      })
      
     }
     else {
      // alert("update");
       this.httpService.putRequest(updateSubCategory, this.subCategoryModel).subscribe((data: any) => {
         if(data){
           this.toastr.successToastr("Update Successfully...",'Success!');
           this.isEdit = true;
           this.getAllSubCategory();
           this.subCategoryForm.reset(); 
           //this.formDirective.resetForm(); 
           }
           else{
             this.toastr.errorToastr("Update Faield...",'Error!');
             this.getAllSubCategory();
             this.isEdit = true;
           }
       }) 
     } 
 
   }

   getAllSubCategory() {
    this.httpService.getRequest(getAllSubCategory).subscribe((data: any) => {
     // console.log(data);
      this.dataSource = new MatTableDataSource(data);
     // this.dataSource.paginator = this.paginator;
     // alert(JSON.stringify(this.dataSource));
    })
  }
  
  getSubCategoryById(element){
    //  alert(JSON.stringify(element));
      this.httpService.getRequest(getSubCategoryById + "/" + element.scId).subscribe((data: any) => {
        // console.log(data); 
       //alert(JSON.stringify(data));
        this.subCategoryModel = data;
        this.subCategoryModel.categoryId=data.categoryMaster.categoryId;
        
       // alert(this.subCategoryModel.categoryId);
        this.isEdit = false;
  
      })
  
    }

    exportToExcel() {
      const ws: xlsx.WorkSheet =
      xlsx.utils.table_to_sheet(this.epltable.nativeElement);
      const wb: xlsx.WorkBook = xlsx.utils.book_new();
      xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
      xlsx.writeFile(wb, 'Subcategory Reports.xlsx');
      }

    applyFilter(event: Event) {
      const filterValue = (event.target as HTMLInputElement).value;
      this.dataSource.filter = filterValue.trim().toLowerCase();
  
      if (this.dataSource.paginator) {
        this.dataSource.paginator.firstPage();
      }
    }
    
}
