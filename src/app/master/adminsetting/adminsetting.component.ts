
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { adminsetting } from '../masterModels';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ToastrManager } from 'ng6-toastr-notifications';
import { Router } from '@angular/router';
import { HttpmethodsService } from 'src/app/service/httpmethods.service';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { insertSetting, updateSetting, getAllsettingCount, getadminById } from '../mastersUrls';
import * as xlsx from 'xlsx';

@Component({
selector: 'app-adminsetting',
templateUrl: './adminsetting.component.html',
styleUrls: ['./adminsetting.component.css']
})
export class AdminsettingComponent implements OnInit {


adminsetting=new adminsetting();
adminForm: FormGroup;
isEdit: boolean = true;


constructor(private formBuilder: FormBuilder, public toastr: ToastrManager, private router: Router, private httpService: HttpmethodsService,) { }
displayedColumns: string[] = ['no','lowStock','Action'];
dataSource = new MatTableDataSource<adminsetting>();
@ViewChild(MatSort, { static: true }) sort: MatSort;
@ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
@ViewChild('epltable', { static: false }) epltable: ElementRef;

ngOnInit() {

this.adminForm = this.formBuilder.group({
// categoryName: new FormControl('', [Validators.required, Validators.pattern("[a-zA-Z\g]* $"),]),
lowStock: new FormControl('', [Validators.required]),


})

this.getAllStock();
}

addCategory(){
if (this.isEdit) {
// alert("Add")
this.httpService.postRequest(insertSetting, this.adminsetting).subscribe((data: any) => {
if (data) {
this.toastr.successToastr("Save Successfully...", 'Success!');
this.isEdit = true;
this.getAllStock();
this.adminForm.reset();
}
else {
this.toastr.errorToastr("Save Faield...", 'Error!');
this.getAllStock();

this.isEdit = true;
}
})
}
else {
// alert("update");
this.httpService.putRequest(updateSetting, this.adminsetting).subscribe((data: any) => {
if (data) {
this.toastr.successToastr("Update Successfully...", 'Success!');
this.isEdit = true;
this.getAllStock();
this.adminForm.reset();
}
else {
this.toastr.errorToastr("Update Faield...", 'Error!');
this.getAllStock();
this.isEdit = true;
}
})
}

}

getAllStock() {
this.httpService.getRequest(getAllsettingCount).subscribe((data: any) => {
// console.log(data);
// alert(JSON.stringify(data));
this.dataSource = new MatTableDataSource(data);
this.dataSource.paginator = this.paginator;
//ng serve alert(JSON.stringify(this.dataSource));
})
}


getLowstockById(element) {
// this.dataService.dataObj=element;
// alert(JSON.stringify(this.dataService.dataObj))
//alert(JSON.stringify(element.categoryId));
this.httpService.getRequest(getadminById + "/" + element.adminId).subscribe((data: any) => {
// console.log(data);
this.adminsetting = data;
this.isEdit = false;

})

}

exportToExcel() {
    const ws: xlsx.WorkSheet =
    xlsx.utils.table_to_sheet(this.epltable.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'Low Stock Reports.xlsx');
    }


    
    applyFilter(event){
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
    
        if (this.dataSource.paginator) {
          this.dataSource.paginator.firstPage();
        }
      }
}