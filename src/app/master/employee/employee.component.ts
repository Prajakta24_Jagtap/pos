import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { employeeMaster } from '../masterModels';
import { ToastrManager } from 'ng6-toastr-notifications';
import { HttpmethodsService } from 'src/app/service/httpmethods.service';
import { getAllEmployee, updateEmployee, createEmployee, getEmployeeById, fileUpload, checkEmployeeMobileNumberURL, checkEmployeeMail, employeeSelectAllTrue, employeeUnselectAllTrue } from '../mastersUrls';
import * as xlsx from 'xlsx';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild('epltable', { static: false }) epltable: ElementRef;

  employeeForm: FormGroup;
  employeeModel= new employeeMaster();
  isEdit: boolean = true;
  fileToUpload: File = null;
  //privilege : boolean=false;
  checked : any;
  mobileMsg: string;
  textColorMobile: string;
  mobFlag: boolean;
  mailMsg: string;
  textColorEmail: string;
  emailFlag: boolean;
  maxdate: any;
  hide: boolean = true;
  buttonDisabled: boolean = true;

  
  constructor(private formBuilder: FormBuilder,public toastr: ToastrManager, private httpService: HttpmethodsService) { }
  //displayedColumns: string[] = ['no', 'countryname', 'statename', 'status','Action'];
  displayedColumns: string[] = ['no','empId','categoryname','subcategoryname','brandname', 'countryname','unitname','pricename','statename','photo', 'status','Action'];

  dataSource = new MatTableDataSource<employeeMaster>();


  

  ngOnInit() {
    this.employeeForm = this.formBuilder.group({
      employeeId:new FormControl(''),
      fullName: new FormControl('', [Validators.required]),
      mobileNumber:new FormControl('', [Validators.required]),
      empMailAddress:new FormControl('', [Validators.required]),
      empPassword:new FormControl('', [Validators.required]),
      employeeDOB:new FormControl('', [Validators.required]),
      employeeRegisterDate:new FormControl('', [Validators.required]),
      empAddress:new FormControl('', [Validators.required]),
      empDesignation:new FormControl('', [Validators.required]),
      empPhoto:new FormControl(''),
      status:new FormControl('', [Validators.required]),
      privilege:new FormControl(''),
      empId:new FormControl('', [Validators.required]),

      //For priviledge
      brand:new FormControl(''),
      category:new FormControl(''),
      subcategory:new FormControl(''),
      product:new FormControl(''),
      unit:new FormControl(''),
      employee:new FormControl(''),
      currentstock:new FormControl(''),
      saleform:new FormControl(''),
      stock:new FormControl(''),
      stockhistory:new FormControl(''),
      invoicenowisedetails:new FormControl(''),
      adminsetting:new FormControl(''),
      changepwd:new FormControl(''),


    })
    this.dataSource.sort = this.sort;
    // this.dataSource.paginator = this.paginator;
   // this.getActiveCategory();
   //this.employeeModel.employeeDOB = formatDate(this.employeeModel.employeeDOB, 'dd-MM-yyyy', 'en');
   this.maxdate = formatDate(new Date, 'yyyy-MM-dd', 'en');
   this.getAllEmployee();

   this.employeeModel.privilege=false;
   this.buttonDisabled=true;

  //  this.employeeModel.employeeId=parseInt(sessionStorage.getItem('employeeId'));
  //  alert("this.employeeModel.employeeId="+JSON.stringify(this.employeeModel.employeeId))
  //  this.employeeModel.category=true;
  //  this.employeeModel.subcategory=true;
  //  this.employeeModel.brand=true;
  //  this.employeeModel.product=true;

  }

  applyFilter(event){
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getAllEmployee() {
    this.httpService.getRequest(getAllEmployee).subscribe((data: any) => {
      //console.log(data);
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
     // alert(JSON.stringify(this.dataSource));
    })
  }

  btnEnable(){
   // alert("btn enable")
    this.buttonDisabled=false;
  }

  addEmployee(){
    //  alert("Emp Model="+JSON.stringify(this.employeeModel))
   
     if (this.isEdit) {
      // alert("Add")
       this.httpService.postRequest(createEmployee, this.employeeModel).subscribe((data: any) => {
        this.buttonDisabled=true;
         if(data){
          this.buttonDisabled=true;
         this.toastr.successToastr("Save Successfully...",'Success!');
         this.isEdit = true;
         this.mobileMsg="";
         this.mailMsg="";
         this.getAllEmployee();
       //  setTimeout(() => this.formGroupDirective.resetForm(), 200);
     //  this.getAllProduct();

    

         this.employeeForm.reset(); 
         this.employeeForm.markAsPristine();
         }
         else{
           this.toastr.errorToastr("Save Faield...",'Error!');
           //this.getAllCategory();
          this.getAllEmployee();
           this.isEdit = true;
         }
       })
     }
    
     else {
      // alert("update");
       this.buttonDisabled=false;
       this.httpService.putRequest(updateEmployee, this.employeeModel).subscribe((data: any) => {
        this.buttonDisabled=true;
         if(data){
          this.buttonDisabled=true;
           this.toastr.successToastr("Update Successfully...",'Success!');
           this.isEdit = true;
          this.getAllEmployee();
           this.employeeForm.reset(); 
      this.employeeForm.markAsPristine();
      
      // setTimeout(() => {
      //   location.reload();
      //   }, 100);
           //this.formDirective.resetForm(); 
           }
           else{
             this.toastr.errorToastr("Update Faield...",'Error!');
             this.getAllEmployee();
             this.isEdit = true;
           }
       }) 
     } 
 
   }
 
   getEmployeeById(element){
    //  alert(JSON.stringify(element));
    this.buttonDisabled=false;
      this.httpService.getRequest(getEmployeeById + "/" + element.employeeId).subscribe((data: any) => {
        // console.log(data); 
       //alert(JSON.stringify(data));
      
      //  this.employeeModel=null;
        this.employeeModel=data;
        //  this.employeeModel.empId=data.empId;
        //  this.employeeModel.fullName=data.fullName;
        //  this.employeeModel.mobileNumber=data.mobileNumber;
        //  this.employeeModel.empMailAddress=data.empMailAddress;
        //  this.employeeModel.empPassword=data.empPassword;
        //  this.employeeModel.employeeDOB=data.employeeDOB;

        //  this.employeeModel.employeeRegisterDate=data.employeeRegisterDate;

        //  this.employeeModel.empAddress=data.empAddress;
        //  this.employeeModel.empDesignation=data.empDesignation;
        //  this.employeeModel.empPhoto=data.empPhoto;
        //  this.employeeModel.status=data.status;

         this.employeeModel.privilege=data.privilege;
         this.employeeModel.brand=data.brand;
         this.employeeModel.category=data.category;
         this.employeeModel.subcategory=data.subcategory;
         this.employeeModel.product=data.product;
         this.employeeModel.unit=data.unit;
         this.employeeModel.employee=data.employee;
         this.employeeModel.currentstock=data.currentstock;
         this.employeeModel.saleform=data.saleform;
         this.employeeModel.stock=data.stock;
         this.employeeModel.stockhistory=data.stockhistory;
         this.employeeModel.invoicenowisedetails=data.invoicenowisedetails;
         this.employeeModel.adminsetting=data.adminsetting;
         this.employeeModel.changepwd=data.changepwd;
     
        this.isEdit = false;
       // this.productModel.categoryId=data.categoryMaster.categoryId;
       
        //console.log("Obj   "+JSON.stringify(this.employeeModel));
        //this.isEdit = false;
        
  
      })
  
    }

    selectAllFun(){
      if(this.employeeModel.privilege){
        //alert("checked all")

        // this.httpService.putRequest(employeeSelectAllTrue,this.employeeModel).subscribe((data: any) => {
        //   if(data==true){
        //   }
        //   else{
        //     //this.toastr.successToastr('Checked all unsuccessfully...!', 'Success!', {timeout : 500});

        //   }
        // })

        this.employeeModel.privilege=true;
        this.employeeModel.category=true;
        this.employeeModel.subcategory=true;
        this.employeeModel.product=true;
        this.employeeModel.brand=true;
        this.employeeModel.unit=true;
        this.employeeModel.employee=true;
        this.employeeModel.currentstock=true;
        this.employeeModel.saleform=true;
        this.employeeModel.stock=true;
        this.employeeModel.stockhistory=true;
        this.employeeModel.invoicenowisedetails=true;
        this.employeeModel.adminsetting=true;
        this.employeeModel.changepwd=true;
      }
      else{
       
       // alert("unchecked all")
        
        // this.httpService.putRequest(employeeUnselectAllTrue,this.employeeModel).subscribe((data: any) => {
        //   if(data==true)
        //   {
         
        //   }
        //   else{
        //  //   this.toastr.successToastr('Unchecked all unsuccessfully...!', 'Success!', {timeout : 500});

        //   }
        // })  
        this.employeeModel.privilege=false;
        this.employeeModel.category=false;
        this.employeeModel.subcategory=false;
        this.employeeModel.product=false;
        this.employeeModel.brand=false;
        this.employeeModel.unit=false;
        this.employeeModel.employee=false;
        this.employeeModel.currentstock=false;
        this.employeeModel.saleform=false;
        this.employeeModel.stock=false;
        this.employeeModel.stockhistory=false;
        this.employeeModel.invoicenowisedetails=false;
        this.employeeModel.adminsetting=false;
        this.employeeModel.changepwd=false;
      }
    }

    handleFileInput(File: FileList) {
      //alert("upload file")
      this.fileToUpload = File.item(0);
      this.httpService.fileUpload(fileUpload, this.fileToUpload).subscribe((
        data: any) => {
        this.employeeModel.empPhoto = data.path;
      })
    }

    checkMobileNumber(mobileNumber) {
      if (mobileNumber.length == 10) {
      this.httpService.getRequest(checkEmployeeMobileNumberURL + "/" + mobileNumber).subscribe((data: any) => {
      if (data == true) {
        this.mobileMsg = "Mobile Number Already Exists";
      this.textColorMobile = "Red";
      
      this.mobFlag = false;

      
      
      
      }
      else {
        // this.toastr.errorToastr("Enter 10 digit Valid Mobile Number")
        this.mobileMsg = "Valid Mobile No ";
        this.textColorMobile = "green";
        this.mobFlag = true;
      
      }
      })
      }
      }
      
      
      checkMail() {
        this.httpService.getRequest(checkEmployeeMail + "?empMailAddress=" + this.employeeModel.empMailAddress).subscribe((data: any) => {
      // this.httpService.getRequest(checkEmployeeMail+"/"+this.employeeModel.empMailAddress).subscribe((data: any) => {
      if (data == true) {
      this.mailMsg = "Email Already Exists ";
      this.textColorEmail = "Red";
      this.emailFlag = false;
      
      }
      else {
      this.mailMsg = "Valid Mail Address";
      this.textColorEmail = "green";
      this.emailFlag = true;
      }
      })
      }
    
      exportToExcel() {
        const ws: xlsx.WorkSheet =
        xlsx.utils.table_to_sheet(this.epltable.nativeElement);
        const wb: xlsx.WorkBook = xlsx.utils.book_new();
        xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
        xlsx.writeFile(wb, 'Employee Reports.xlsx');
        }
}
