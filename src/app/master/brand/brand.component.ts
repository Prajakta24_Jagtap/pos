import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { brandMaster, nameexistmaster } from '../masterModels';
import { ToastrManager } from 'ng6-toastr-notifications';
import { HttpmethodsService } from 'src/app/service/httpmethods.service';
import { createBrand, updateBrand, getAllBrand, getBrandById, checkAlreadyExistName } from '../mastersUrls';
import * as xlsx from 'xlsx';

@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.css']
})
export class BrandComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild('epltable', { static: false }) epltable: ElementRef;
  
  brandForm: FormGroup;
  brandModel= new brandMaster();
  isEdit: boolean = true;
  categoryActiveList: any[];
  nameexistmaster=new nameexistmaster();

  constructor(private formBuilder: FormBuilder,public toastr: ToastrManager, private httpService: HttpmethodsService) { }
  displayedColumns: string[] = ['no', 'countryname', 'status','Action'];
  dataSource = new MatTableDataSource<brandMaster>();


  

  ngOnInit() {
    this.brandForm = this.formBuilder.group({
      brandName: new FormControl('', [Validators.required]),
      brandStatus:new FormControl('',[Validators.required]),


    })
    this.dataSource.sort = this.sort;
    // this.dataSource.paginator = this.paginator;
  //  this.getActiveCategory();
    this.getAllBrand();
  }

  addBrand(){
    if (this.isEdit) {
      // alert("Add")




      this.nameexistmaster.brandName=this.brandModel.brandName;
      this.httpService.postRequest(checkAlreadyExistName,this.nameexistmaster).subscribe((data: any) => {
        if(data)
        {
         

        

       this.httpService.postRequest(createBrand, this.brandModel).subscribe((data: any) => {
        if (data) {
          this.toastr.successToastr("Save Successfully...", 'Success!');
          this.isEdit = true;
         this.getAllBrand();
          this.brandForm.reset();
        }
        else {
          this.toastr.errorToastr("Save Faield...", 'Error!');
         this.getAllBrand();

          this.isEdit = true;
        }
      })

        }
        else
        {
          this.toastr.errorToastr("Already Exist Brand Name ", 'Error!');
        }
      })









     }
     else {
      // alert("update");
       this.httpService.putRequest(updateBrand, this.brandModel).subscribe((data: any) => {
         if (data) {
           this.toastr.successToastr("Update Successfully...", 'Success!');
           this.isEdit = true;
          this.getAllBrand();
           this.brandForm.reset();
         }
         else {
           this.toastr.errorToastr("Update Faield...", 'Error!');
          this.getAllBrand();
           this.isEdit = true;
         }
       })
     }
 
  } 

  getAllBrand() {
    this.httpService.getRequest(getAllBrand).subscribe((data: any) => {
      // console.log(data);
    //  alert(JSON.stringify(data));
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
      //ng serve alert(JSON.stringify(this.dataSource));
    })
  }

  getBrandById(element) {
 //   this.dataService.dataObj=element;
  //  alert(JSON.stringify(this.dataService.dataObj))
    //alert(JSON.stringify(element.categoryId));
    this.httpService.getRequest(getBrandById + "/" + element.brandId).subscribe((data: any) => {
    //  console.log(data);
      this.brandModel = data;
      this.isEdit = false;

    })

  }

  exportToExcel() {
    const ws: xlsx.WorkSheet =
    xlsx.utils.table_to_sheet(this.epltable.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'Brand Reports.xlsx');
    }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  
}
