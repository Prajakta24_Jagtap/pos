
export const createCategory ="/category";
export const updateCategory ="/category";
export const getAllCategory="/category/getList";
export const getCategoryById="/category/getById"; 
export const getActiveCategory="/category/active";
export const checkAlreadyExistName="/category/checkAlreadyExistName";

export const createSubCategory="/subcategory"; 
export const updateSubCategory="/subcategory";
export const getAllSubCategory ="/subcategory";
export const getSubCategoryById ="/subcategory/";
export const getActiveSubCategory="/subcategory/active";

export const createBrand="/brand"; 
export const updateBrand="/brand";
export const getAllBrand ="/brand/getList";
export const getBrandById ="/brand/getById/";
export const getActiveBrand="/brand/activebrand";


export const createUnit="/unit"; 
export const updateUnit="/unit";
export const getAllUnit ="/unit/getList";
export const getUnitById ="/unit/editUnit/";
//export const getActiveUnit="/unit/activeUnit";

export const createProduct="/product"; 
export const updateProduct="/product";
export const getAllProduct ="/product/getAllList";
export const getProductById ="/product";
export const getActiveProduct="/product/activeproduct";
export const getFalseLooseUnit="/unit/getFalseLooseUnit";
export const getActiveLooseUnit="/unit/getTrueLooseUnit";


export const createEmployee="/employee"; 
export const updateEmployee="/employee";
export const getAllEmployee ="/employee";
export const getEmployeeById ="/employee/";
export const employeeForgotPassword="/employee/forgotPassword";
export const employeeUpdatepassword="/employee/changePassword"; 
//export const getActiveEmployee="/employee/active;
export const fileUpload="/uploadFile";

export const checkEmployeeMobileNumberURL="/employee/checkEmployeeMobileNumber";
export const checkEmployeeMail="/employee/checkEmployeeMailId";
export const employeeSelectAllTrue="/employee/selectAllTrue";
export const employeeUnselectAllTrue="/employee/unselectAllFalse";


export const login="/employee/login";

export const generateOtp="/employee/generateotp";
export const validateOtp="/employee/validateotp";

 export const getRetailList="/product/searchRetail";
 export const getWholesaleList="/product/searchWholeSale";
export const getNameByMobNo="/customer/search";
export const createOrder="/order"; 
// export const getSaleById="/sale/";

export const getInvoiceNo="/order/genrateInvoiceNumber"; 
export const invoiceNoWiseOrder="/order/InvoiceNoWiseOrder";

export const createStock="/stock";
export const getAllStock="/stock/getAllStockList";
// export const getByProduct="/stock/getByProductId";



export const getStockHistoryRecords="/productWiseStockRecord/dateWiseRecord";
export const getYesterdaysStockHistory="/productWiseStockRecord/getYesterdaysProductsHistory";
export const returnQty="/orderWiseReturnProduct";

export const getInvoiceNoList="/order/InvoiceNumberWiseSearch";

export const getadminById="/adminSetting/editSetting";
export const insertSetting="/adminSetting";
export const updateSetting="/adminSetting";
export const getAllsettingCount="/adminSetting/getAllsettingCount";

export const getByBarcodeDataRetail="/product/searchProductBarcodeWiseRetail"
export const getByBarcodeData="/product/searchProductBarcodeWise";
export const getByBrandId="/product/getByBrandId";
export const getActiveSubCategorycatwise="/subcategory/getList";

