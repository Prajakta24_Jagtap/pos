import { ElementRef } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ToastrManager } from 'ng6-toastr-notifications';
import { formatDate } from '@angular/common';
import { HttpmethodsService } from 'src/app/service/httpmethods.service';
import { invoiceNoWiseDetailsMaster, prodMaster } from '../masterModels';
import { createOrder, getInvoiceNoList, getNameByMobNo, getRetailList, getWholesaleList, invoiceNoWiseOrder, returnQty } from '../mastersUrls';
import * as xlsx from 'xlsx';

@Component({
  selector: 'app-invoicenowisedetails',
  templateUrl: './invoicenowisedetails.component.html',
  styleUrls: ['./invoicenowisedetails.component.css']
})
export class InvoicenowisedetailsComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild('epltable', { static: false }) epltable: ElementRef;

   //For change focus
   @ViewChild("qty",{static:true}) qtyField: ElementRef;
   qty: number;
  invNo: any;
  tableQty: number;
  loosecheck: boolean;
 
   editQty():void{
     this.qtyField.nativeElement.focus();
   }
 
  //  @ViewChild("productFocus",{static:true}) productNameField:ElementRef;
  //  setFocusToProductName():void{
  //    this.productNameField.nativeElement.focus();
  //  }
 

  invoiceNoWiseForm: FormGroup;
  invoiceNoWiseModel= new invoiceNoWiseDetailsMaster();
  isLoad:boolean=false;
  isEdit: boolean = true;
  retailList: any[];
  wholesaleList: any[];
  mobileNoList:any[];
  invoiceNoList:any[];
  row= new prodMaster();
  productList =new Array<prodMaster>();
 
  showLooseUnit: boolean=false;


  constructor(private formBuilder: FormBuilder,public toastr: ToastrManager, private httpService: HttpmethodsService) { }
  displayedColumns: string[] = ['no','productname','qty','unit','brandname','discountPerc','discount','purchasePrice','rate', 'countryname','Action'];

  dataSource = new MatTableDataSource<invoiceNoWiseDetailsMaster>();




  ngOnInit() {
    this.invoiceNoWiseForm = this.formBuilder.group({
      // stockId:new FormControl(''),
      invoiceNo:new FormControl('', [Validators.required]),
      saleId:new FormControl(''),
      invoiceDate:new FormControl('', [Validators.required]),
      saleType:new FormControl('', [Validators.required]),
      cashier:new FormControl('', [Validators.required]),
    payType:new FormControl('', [Validators.required]),
    custMobileNumber:new FormControl('', [Validators.required]),
    customerName:new FormControl('', [Validators.required]),
    productName:new FormControl(''),
    productDescription:new FormControl('', [Validators.required]),
    qty:new FormControl('', [Validators.required]),
    mrp:new FormControl('', [Validators.required]),
    rate:new FormControl('', [Validators.required]),
    finalAmount:new FormControl('', [Validators.required]),
    unitName:new FormControl('', [Validators.required]),
    discountPerc:new FormControl('', [Validators.required]),
    discount:new FormControl('', [Validators.required]),

    employeeId:new FormControl(''),
    currentStock:new FormControl(''), 
    looseUnit:new FormControl(''),
    unitDropDown:new FormControl(''),
    
    purchasePrice:new FormControl(''),
    totalPurchasePrice:new FormControl(''),


    })


    this.dataSource.sort = this.sort;
    // this.dataSource.paginator = this.paginator;
    this.invoiceNoWiseModel.invoiceNo=sessionStorage.getItem('invoiceNo');

         // alert("Sachin"+sessionStorage.getItem('isLoad'));
          //alert("Sachin"+sessionStorage.getItem('invoiceNo'));
    if(sessionStorage.getItem("isLoad")!="false"){

      // alert("Sachin"+sessionStorage.getItem("isLoad"));
    this.getInvoiceNowiseDetails();
    sessionStorage.setItem("isLoad","false");
    }

  //   this.invoiceNoWiseModel.invoiceDate = formatDate(new Date(), 'yyyy-MM-dd', 'en');

  //   this.invoiceNoWiseModel.saleType="Retail";

  //   this.invoiceNoWiseModel.payType="Cash";
    // this.invoiceNoWiseModel.looseUnit=false;
  //   this.invoiceNoWiseModel.unitDropDown="gm";

  //   this.invoiceNoWiseModel.cashier=sessionStorage.getItem('fullName');
  //  // alert("fname="+JSON.stringify(this.invoiceNoWiseModel.cashier))
  //  this.invoiceNoWiseModel.employeeId=parseInt(sessionStorage.getItem('employeeId'));
    
  }

  getInvoiceNoList(invoiceNo){
    // alert("getname="+JSON.stringify(invoiceNo))  
    this.invoiceNoWiseModel.invoiceNo=invoiceNo;
    // alert("getname="+JSON.stringify(this.invoiceNoWiseModel.custMobileNumber));
     this.httpService.getRequest(getInvoiceNoList+"/"+this.invoiceNoWiseModel.invoiceNo).subscribe((data: any) => {
       // console.log(data);
       this.invoiceNoList=data;
        //this.invoiceNoWiseModel.customerName = data.customerName;
        
        // alert("mobileNoList="+JSON.stringify( this.mobileNoList))
        })
   }

  getInvoiceNowiseDetails(){
    // alert("getstock history")
    //alert("invoiceNoWiseModel.invoiceNo="+JSON.stringify(this.invoiceNoWiseModel.invoiceNo))
     this.httpService.getRequest(invoiceNoWiseOrder+"/"+this.invoiceNoWiseModel.invoiceNo).subscribe((data: any) => {
      // console.log(data);
       //alert("inv no wise details="+JSON.stringify(data))
       this.invoiceNoWiseModel=data;
       this.invNo=data.invoiceNo;
       this.tableQty=data.totalQuantity;
      
       this.dataSource = new MatTableDataSource(data.productList);

       this.dataSource.paginator = this.paginator;

       //alert("datasource="+JSON.stringify(this.dataSource))
       })
   }
 
   applyFilter(event){
     const filterValue = (event.target as HTMLInputElement).value;
     this.dataSource.filter = filterValue.trim().toLowerCase();
 
     if (this.dataSource.paginator) {
       this.dataSource.paginator.firstPage();
     }
   }

   getByIdSearch(data){
   // this.loosecheck=data.productList.looseUnit;
    // if(data.productList.looseUnit==true)
    // {
    //       this.showLooseUnit=true;
    //       this.invoiceNoWiseModel.looseUnit=true;
    // }
    // else{
    //   this.showLooseUnit=false;
    //   this.invoiceNoWiseModel.looseUnit=false;
    // }
    this.row = data;
   // console.log(this.row);
    
  }



  addDetails(row){
  //   //alert(JSON.stringify(row))
  //   this.invoiceNoWiseModel.productList.push(this.row);
  // //  this.dataSource.data=this.invoiceNoWiseModel.productList;
  //  // alert(JSON.stringify( this.dataSource.data))
  //   this.row= new prodMaster();
  //   this.invoiceNoWiseForm.controls['productName'].reset();
  //   this.calculation();
  if(row.qty==0 || row.qty==null )
  {
    alert("Please Enter Quantity !!");
  }
  if(row.qty!=null)
  {

  
   this.httpService.postRequest(returnQty, this.row).subscribe((data: any) => {


        if(data){
        this.toastr.successToastr("Product Return Successfully...",'Success!');
        //this.isEdit = true;
       // this.getAllCategory();
      //  setTimeout(() => this.formGroupDirective.resetForm(), 200);
      //this.getAllProduct();
        
          this.getInvoiceNowiseDetails();
      this.row=new prodMaster();
      this.invoiceNoWiseForm.reset(); 
         this.invoiceNoWiseForm.markAsPristine();
        // this.saleForm.markAsPristine();
        // setTimeout(() => {
        //   location.reload();
        // }, 100);
        // this.saleModel.cashier=sessionStorage.getItem('fullName');
        // this.getInvoiceNo();

        }
        else{
          this.toastr.errorToastr("Save Faield...",'Error!');
          //this.getAllCategory();
        // this.getAllProduct();
         // this.isEdit = true;
          // setTimeout(() => {
          //   location.reload();
          // }, 100);
        }
      })
    }
  
 
   }
  
   editDetails(element)
   {
    // alert("element="+JSON.stringify(element))

     this.row= element;
     if(element.looseUnit==true)
    {
      //alert("invoiceNoWiseModel.looseUnit"+JSON.stringify(element.looseUnit))
          this.showLooseUnit=true;
          this.row.looseUnit=true;
    }
    else{
      this.showLooseUnit=false;
      this.row.looseUnit=false;
    }
    // this.invoiceNoWiseModel.productList.splice(i,1);
    // this.calculation();
    // this.dataSource.data= this.invoiceNoWiseModel.productList;
   }

   removeData(index)
  {
    this.invoiceNoWiseModel.productList.splice(index,1);
    this.calculation();
   // this.dataSource.data=this.invoiceNoWiseModel.productList;
  }
  getProdList(event) {
    this.invoiceNoWiseModel.productName = event.target.value;
    // alert(this.invoiceNoWiseModel.productName)
    if(this.invoiceNoWiseModel.saleType=="Retail"){
    this.httpService.getRequest(getRetailList+"/"+this.invoiceNoWiseModel.productName).subscribe((data: any) => {
      // console.log(data);
      // this.invoiceNoWiseModel = data;
       this.retailList=data;
      // this.invoiceNoWiseModel.orderDetailReqDtoList=this.retailList;
      // alert(JSON.stringify( this.retailList))
       })
      }
      else if(this.invoiceNoWiseModel.saleType=="Wholesale"){
        this.httpService.getRequest(getWholesaleList+"/"+this.invoiceNoWiseModel.productName).subscribe((data: any) => {
                // console.log(data);
              //   this.invoiceNoWiseModel = data;
                 this.retailList=data;
                // this.invoiceNoWiseModel.orderDetailReqDtoList=this.retailList;
                 //alert(JSON.stringify( this.retailList))
                 })
         }
   
    
  }
  calFinalAmt(){
    if(this.showLooseUnit==false)
      {
        if(this.row.qty > this.tableQty)
        {
          // alert("Quantity is greater than current stock")
          alert('Quantity is greater than Return Quantity!');
        }
      }

      if(this.row.looseUnit){
      //alert("loose");
      
      if(this.row.unitDropDown=="gm"){
        this.qty= this.row.qty/1000;
        this.row.finalAmount=Math.round(this.qty*this.row.rate);

      }
      if(this.row.unitDropDown=="KG"){
        this.row.finalAmount=Math.round(this.row.qty*this.row.rate);
      }
      if(this.row.unitDropDown=="ml"){
        this.qty=this.row.qty/1000;
        this.row.finalAmount=Math.round(this.qty*this.row.rate);
      }
      if(this.row.unitDropDown=="L"){
        this.row.finalAmount=Math.round(this.row.qty*this.row.rate);
      }
    }
    else{
      //alert("not loose");
      this.row.finalAmount= Math.round(this.row.qty*this.row.rate);
      
    }
    }



  getNameByMobNoo(mobNo){
    // alert("getname="+JSON.stringify(mobNo))  
    this.invoiceNoWiseModel.custMobileNumber=mobNo;
    // alert("getname="+JSON.stringify(this.invoiceNoWiseModel.custMobileNumber));
     this.httpService.getRequest(getNameByMobNo+"/"+this.invoiceNoWiseModel.custMobileNumber).subscribe((data: any) => {
       // console.log(data);
       this.mobileNoList=data;
        //this.invoiceNoWiseModel.customerName = data.customerName;
        
        // alert("mobileNoList="+JSON.stringify( this.mobileNoList))
        })
   }
   setName(data1){
     this.invoiceNoWiseModel.customerName=data1.customerName;
   //  alert("customer name="+JSON.stringify( this.invoiceNoWiseModel.customerName))
   }

   
  addOrderDetails(){
   // alert("addddd")
    this.httpService.postRequest(createOrder, this.invoiceNoWiseModel).subscribe((data: any) => {
      if(data){
      this.toastr.successToastr("Save Successfully...",'Success!');
      this.isEdit = true;
     // this.getAllCategory();
    //  setTimeout(() => this.formGroupDirective.resetForm(), 200);
    //this.getAllProduct();
    
      this.invoiceNoWiseForm.reset(); 
      this.invoiceNoWiseForm.markAsPristine();
      setTimeout(() => {
        location.reload();
      }, 100);
      // this.invoiceNoWiseModel.cashier=sessionStorage.getItem('fullName');
      // this.getInvoiceNo();

      }
      else{
        this.toastr.errorToastr("Save Faield...",'Error!');
        //this.getAllCategory();
      // this.getAllProduct();
        this.isEdit = true;
        setTimeout(() => {
          location.reload();
        }, 100);
      }
    })
  }
  calculation(){
    
     
 this.invoiceNoWiseModel.totalItems=0;
 this.invoiceNoWiseModel.totalQuantity=0;
 this.invoiceNoWiseModel.totalMrp=0;
 this.invoiceNoWiseModel.totalRate=0;
 this.invoiceNoWiseModel.totalPurchasePrice=0;
 this.invoiceNoWiseModel.totalFinalAmt=0;
 this.invoiceNoWiseModel.youSave=0;
// this.saleModule.productList
    for (let j = 0; j < this.invoiceNoWiseModel.productList.length; j++) {
      //this.totalPrintedAmt =  this.orderProductList[j].totalGST - this.totalPrintedAmt ;

     this.invoiceNoWiseModel.totalMrp = this.invoiceNoWiseModel.totalMrp + this.invoiceNoWiseModel.productList[j].mrp;
     this.invoiceNoWiseModel.totalItems=this.invoiceNoWiseModel.totalItems+1;
     this.invoiceNoWiseModel.totalQuantity= this.invoiceNoWiseModel.totalQuantity+this.invoiceNoWiseModel.productList[j].qty;
     this.invoiceNoWiseModel.totalRate=this.invoiceNoWiseModel.totalRate+this.invoiceNoWiseModel.productList[j].rate;
     this.invoiceNoWiseModel.totalPurchasePrice=this.invoiceNoWiseModel.totalPurchasePrice+this.invoiceNoWiseModel.productList[j].purchasePrice;
     this.invoiceNoWiseModel.totalFinalAmt=this.invoiceNoWiseModel.totalFinalAmt+this.invoiceNoWiseModel.productList[j].finalAmount;
    // alert("total Items:"+this.invoiceNoWiseModel.totalItems+ " Total Qty:"+ this.invoiceNoWiseModel.totalQuantity+"total Mrp:"+this.invoiceNoWiseModel.totalMrp+"total price:"+this.invoiceNoWiseModel.totalRate+"Total F Amt"+this.invoiceNoWiseModel.totalFinalAmt)

   
    }

    this.invoiceNoWiseModel.youSave=this.invoiceNoWiseModel.totalMrp-this.invoiceNoWiseModel.totalRate;
  }

  discountPercentageWise(){
    this.row.discount=this.row.mrp*this.row.discountPerc/100;
    this.row.rate=this.row.mrp-this.row.discount;

    this.calFinalAmt();
  }

  discountAmountWise(){
    this.row.discountPerc=this.row.discount*100/this.row.mrp;
    this.row.rate=this.row.mrp-this.row.discount;

    this.calFinalAmt();
  }

  FinalAmountWise(){

    this.row.discount=this.row.mrp-this.row.rate;
    this.row.discountPerc=this.row.discount*100/this.row.mrp;

    this.calFinalAmt();
  }

  exportToExcel() {
    const ws: xlsx.WorkSheet =
    xlsx.utils.table_to_sheet(this.epltable.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'Invoice Reports.xlsx');
    }

 

}
