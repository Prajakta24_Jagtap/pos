import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoicenowisedetailsComponent } from './invoicenowisedetails.component';

describe('InvoicenowisedetailsComponent', () => {
  let component: InvoicenowisedetailsComponent;
  let fixture: ComponentFixture<InvoicenowisedetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoicenowisedetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoicenowisedetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
