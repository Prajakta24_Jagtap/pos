import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { HttpmethodsService } from 'src/app/service/httpmethods.service';
import { changePswMaster } from '../masterModels';
  import { employeeUpdatepassword } from '../mastersUrls';

@Component({
  selector: 'app-changepwd',
  templateUrl: './changepwd.component.html',
  styleUrls: ['./changepwd.component.css']
})
export class ChangepwdComponent implements OnInit {
  //newPass: boolean = true;
  // message: string;
  // msgcolor: string;
  //submitBtn: boolean = false;
  //newPassword: string;
  hide: boolean = true;
  changepwdForm : FormGroup;
  changepwdModel=new changePswMaster();
  sort: any;


  constructor(private formBuilder: FormBuilder,public toastr: ToastrManager,private router: Router, private httpService: HttpmethodsService) {
    
   }
  //displayedColumns: string[] = ['no', 'countryname', 'statename', 'status','Action'];
  displayedColumns: string[] = ['no','categoryname','subcategoryname','brandname', 'countryname','Action'];

  dataSource = new MatTableDataSource<changePswMaster>();

  

  ngOnInit() {
    this.changepwdForm = this.formBuilder.group({

      empPassword:new FormControl('', Validators.required),
      newPassword:new FormControl('', Validators.required),
      employeeId:new FormControl(''),
    })
    this.dataSource.sort = this.sort;
    //this.disabledButton='1';
    
  }

 

  // checkPassword(employeeCheckPasswordModel) {
  //   employeeCheckPasswordModel.employeeId = 1;
  //   if (employeeCheckPasswordModel.employeePassword.length > 5) {
  //     this.httpService.postRequest(employeeCheckPassword, employeeCheckPasswordModel).subscribe((data: any) => {
  //       if (data) {
  //         this.newPass = false;
  //         this.message = "Please enter new password"
  //         this.msgcolor = "green"
  //         this.submitBtn = true;
  //       }
  //       else {
  //         this.newPass = true;
  //         this.message = "Please enter valid password";
  //         this.msgcolor = "red"
  //       }
  //     })
  //   }
  //   else {
  //     this.newPass = true;
  //     this.message = "Please enter valid password";
  //     this.msgcolor = "red";
  //   }
  // }



  changePassword() 
  {

    this.changepwdModel.employeeId = parseInt(sessionStorage.getItem('employeeId'));

    if(this.changepwdModel.empPassword == null || this.changepwdModel.newPassword == null)
    {
        alert("Please Enter Password!!")
    }
    else if(this.changepwdModel.empPassword!= null || this.changepwdModel.newPassword!= null)
    {
     // alert("correct")
       this.httpService.postRequest(employeeUpdatepassword, this.changepwdModel).subscribe((data: any) => {
        if(data.status){
        this.changepwdForm.reset();
        this.toastr.successToastr('Password Changed Successfully...!', 'Success!', {timeout : 500});
      
        this.router.navigateByUrl("/login/loginpage")
  
        }
        else{
          this.toastr.successToastr('Password not changed', 'Success!', {timeout : 500});
  
        }
      })
    }
    
  }
}
