import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { nameexistmaster, productMaster } from '../masterModels';
import { ToastrManager } from 'ng6-toastr-notifications';
import { HttpmethodsService } from 'src/app/service/httpmethods.service';
import { getActiveCategory, createProduct, updateProduct, getProductById, getActiveSubCategory, getActiveBrand, getAllProduct, getActiveLooseUnit, getFalseLooseUnit, getActiveSubCategorycatwise, checkAlreadyExistName } from '../mastersUrls';
import * as xlsx from 'xlsx';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild('epltable', { static: false }) epltable: ElementRef;

  productForm: FormGroup;
  productModel= new productMaster();
  nameexistmaster=new nameexistmaster();
  isEdit: boolean = true;
  categoryActiveList: any[];
  subCategoryActiveList:any[];
  brandActiveList:any[];
  unitActiveList:any[];
  width = 2;
  height = 30;
  subCategoryActiveListnew: any[];
  buttonDisabled: boolean = true;



  constructor(private formBuilder: FormBuilder,public toastr: ToastrManager, private httpService: HttpmethodsService) { }
  //displayedColumns: string[] = ['no', 'countryname', 'statename', 'status','Action'];
  displayedColumns: string[] = ['no','categoryname','subcategoryname','brandname', 'countryname','unitname','pricename','wholesaleprice','stock','barcode','status','Action'];

  dataSource = new MatTableDataSource<productMaster>();


  

  ngOnInit() {
    this.productForm = this.formBuilder.group({
      productId:new FormControl(''),
      productName: new FormControl('', [Validators.required]),
      categoryId:new FormControl('', [Validators.required]),
      scId:new FormControl('', [Validators.required]),
      brandId:new FormControl('', [Validators.required]),
      unitId:new FormControl('', [Validators.required]),
      productStatus:new FormControl('', [Validators.required]),
      mrp:new FormControl('', [Validators.required]),
      retailDiscountPerc:new FormControl(''),
      retailDiscount:new FormControl(''),
      wholesaleDiscountPerc:new FormControl(''),
      wholesaleDiscount:new FormControl(''),
      //productPrice:new FormControl('', [Validators.required]),
      productDescription:new FormControl('', [Validators.required]),
      retailPrice:new FormControl('', [Validators.required]),
      wholesalePrice:new FormControl('', [Validators.required]),
      unit:new FormControl('', [Validators.required]),
      purchasePrice:new FormControl('', [Validators.required]),
      looseUnit:new FormControl(''),
     currentStock:new FormControl('', [Validators.required]),
     openingStock:new FormControl('',[Validators.required]),
     barCode:new FormControl('',[Validators.required]),
    })
    this.dataSource.sort = this.sort;
    // this.dataSource.paginator = this.paginator;
    this.getActiveCategory();
    this.getActiveSubCategory();
    this.getAllProduct();
    this.getActiveBrand();
    this.getFalseLooseUnit();

    this.buttonDisabled=true;
  }

  getActiveCategory(){
    this.httpService.getRequest(getActiveCategory).subscribe((data: any) => {
     // console.log(data);
      this.categoryActiveList = data;
      })
  }
  getActiveSubCategory1(category){
    // this.httpService.getRequest(getActiveSubCategorycatwise).subscribe((data: any) => {
    // console.log(data);
    // this.subCategoryActiveList = data;
    // })
    
    // alert("cat id"+JSON.stringify(category.categoryId))
    
    this.httpService.getRequest(getActiveSubCategorycatwise + "/" + category.categoryId).subscribe((data: any) => {
   // console.log(data);
    this.subCategoryActiveListnew = data;
    
    
    })
    
    }
  getActiveSubCategory(){
    this.httpService.getRequest(getActiveSubCategory).subscribe((data: any) => {
     // console.log(data);
      this.subCategoryActiveList = data;
      })
  }

  getActiveBrand(){
    this.httpService.getRequest(getActiveBrand).subscribe((data: any) => {
     // console.log(data);
      this.brandActiveList = data;
      })
  }
  getFalseLooseUnit(){
    this.httpService.getRequest(getFalseLooseUnit).subscribe((data: any) => {
      // console.log(data);
       this.unitActiveList = data;
       })
  }
  
  getActiveLooseUnit(){
    if (this.productModel.looseUnit) {
     // alert("unchk")
      this.getFalseLooseUnit();
    }
    else{
      //alert("chk")
      this.httpService.getRequest(getActiveLooseUnit).subscribe((data: any) => {
         // console.log(data);
          this.unitActiveList = data;
          })
    }
  }

  btnEnable(){
     alert("btn enable")
     this.buttonDisabled=false;
   }

  addProduct(){
    // alert(JSON.stringify(this.subCategoryModel))
     if (this.isEdit) {
      // alert("Add")



      this.nameexistmaster.productName=this.productModel.productName;
      this.httpService.postRequest(checkAlreadyExistName,this.nameexistmaster).subscribe((data: any) => {
        this.buttonDisabled=true;


        if(data)
        {
         
          this.buttonDisabled=true;
          this.httpService.postRequest(createProduct, this.productModel).subscribe((data: any) => {
            if(data){
            this.toastr.successToastr("Save Successfully...",'Success!');
            this.isEdit = true;
           // this.getAllCategory();
          //  setTimeout(() => this.formGroupDirective.resetForm(), 200);
          this.getAllProduct();
            this.productForm.reset(); 
            this.productForm.markAsPristine();
            }
            else{
              this.toastr.errorToastr("Save Faield...",'Error!');
              //this.getAllCategory();
             this.getAllProduct();
              this.isEdit = true;

              this.productForm.reset(); 
              this.productForm.markAsPristine();
            }
          })
        }
        else
        {
          this.toastr.errorToastr("Already Exist Product Name ", 'Error!');
        }
      })


     }
     else {
      // alert("update");
       this.httpService.putRequest(updateProduct, this.productModel).subscribe((data: any) => {
         if(data){
           this.toastr.successToastr("Update Successfully...",'Success!');
           this.isEdit = true;
           this.getAllProduct();
           this.productForm.reset(); 
           //this.formDirective.resetForm(); 
           }
           else{
             this.toastr.errorToastr("Update Faield...",'Error!');
             this.getAllProduct();
             this.isEdit = true;
           }
       }) 
     } 
 
   }

   getAllProduct() {
    this.httpService.getRequest(getAllProduct).subscribe((data: any) => {
    //  console.log(data);
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
     // alert(JSON.stringify(this.dataSource));
    })
  }
  
  getProductById(element){
    //  alert(JSON.stringify(element));
      this.httpService.getRequest(getProductById + "/" + element.productId).subscribe((data: any) => {
        // console.log(data); 
       alert(JSON.stringify(data.subCategoryMaster.scId));
        this.productModel = data;
    
        this.productModel.categoryId=data.categoryMaster.categoryId;
        this.productModel.scId=data.subCategoryMaster.scId;
        this.productModel.brandId=data.brandMaster.brandId;
        this.productModel.unitId=data.unitMaster.unitId;
       // alert(this.subCategoryModel.categoryId);
        this.isEdit = false;
  
      })
  
    }

    retailDiscountPercentageWise(){
     
      this.productModel.retailDiscount=this.productModel.mrp*this.productModel.retailDiscountPerc/100;
      this.productModel.retailPrice=this.productModel.mrp-this.productModel.retailDiscount;

    }

    retailDiscountAmountWise(){
      this.productModel.retailDiscountPerc=this.productModel.retailDiscount*100/this.productModel.mrp;
      this.productModel.retailPrice=this.productModel.mrp-this.productModel.retailDiscount;

    }

    RetailPriceWise(){
  
      this.productModel.retailDiscount=this.productModel.mrp-this.productModel.retailPrice;
      this.productModel.retailDiscountPerc=this.productModel.retailDiscount*100/this.productModel.mrp;
    }

    wholesaleDiscountPercentageWise(){
      this.productModel.wholesaleDiscount=this.productModel.mrp*this.productModel.wholesaleDiscountPerc/100;
      this.productModel.wholesalePrice=this.productModel.mrp-this.productModel.wholesaleDiscount;

    }

    wholesaleDiscountAmountWise(){
      this.productModel.wholesaleDiscountPerc=this.productModel.wholesaleDiscount*100/this.productModel.mrp;
      this.productModel.wholesalePrice=this.productModel.mrp-this.productModel.wholesaleDiscount;

    }

    WholesalePriceWise(){
  
      this.productModel.wholesaleDiscount=this.productModel.mrp-this.productModel.wholesalePrice;
      this.productModel.wholesaleDiscountPerc=this.productModel.wholesaleDiscount*100/this.productModel.mrp;
    }

    exportToExcel() {
      const ws: xlsx.WorkSheet =
      xlsx.utils.table_to_sheet(this.epltable.nativeElement);
      const wb: xlsx.WorkBook = xlsx.utils.book_new();
      xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
      xlsx.writeFile(wb, 'Product Reports.xlsx');
      }

      
    applyFilter(event: Event) {
      const filterValue = (event.target as HTMLInputElement).value;
      this.dataSource.filter = filterValue.trim().toLowerCase();
  
      if (this.dataSource.paginator) {
        this.dataSource.paginator.firstPage();
      }
    }

}
