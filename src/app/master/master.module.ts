import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MasterRoutingModule } from './master-routing.module';
import { CategoryComponent } from './category/category.component';
import { SubcategoryComponent } from './subcategory/subcategory.component';
import { UnitComponent } from './unit/unit.component';
import { BrandComponent } from './brand/brand.component';
import { MatPaginatorModule, MatFormFieldModule, MatInputModule, MatCardModule, MatDatepickerModule, MatSelectModule, MatAutocompleteModule, MatSlideToggleModule, MatProgressSpinnerModule, MatNativeDateModule, MatRadioModule, MatCheckboxModule, MatSidenavModule, MatDividerModule, MatDialogModule, MatButtonModule, MatIconModule, MatTableModule, MatSortModule } from '@angular/material';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ProductComponent } from './product/product.component';
import { EmployeeComponent } from './employee/employee.component';
import { ChangepwdComponent } from './changepwd/changepwd.component';
import { SaleformComponent } from './saleform/saleform.component';
import { StockComponent } from './stock/stock.component';
import { StockhistoryComponent } from './stockhistory/stockhistory.component';
import { InvoicenowisedetailsComponent } from './invoicenowisedetails/invoicenowisedetails.component';
import { AdminsettingComponent } from './adminsetting/adminsetting.component';
import { CurrentstockComponent } from './currentstock/currentstock.component';
import { NgxBarcodeModule } from 'ngx-barcode';


@NgModule({
  declarations: [CategoryComponent, SubcategoryComponent, UnitComponent, BrandComponent, ProductComponent, EmployeeComponent, ChangepwdComponent, SaleformComponent, StockComponent, StockhistoryComponent, InvoicenowisedetailsComponent, AdminsettingComponent, CurrentstockComponent],
  imports: [
    CommonModule,
    MasterRoutingModule,
    MatPaginatorModule,
    SharedModule, 
    MatFormFieldModule,
    MatInputModule,
    MatCardModule, 
    MatDatepickerModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatSlideToggleModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatDividerModule,
    MatDialogModule,
    MatButtonModule,
    MatButtonModule,
    MatDialogModule, 
    MatIconModule,
    MatSidenavModule,
    MatDividerModule,
    MatDialogModule,
    MatButtonModule,
    MatSelectModule,
    MatTableModule,
    MatSortModule,
    NgxBarcodeModule
  ]
})
export class MasterModule { }
