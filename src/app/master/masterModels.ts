export class categoryMaster{

    categoryId:number;
    categoryName:string;
    categoryStatus:string;
    
}

export class subCategoryMaster {
    // [x: string]: any;
    scId:number;
    subcategoryName:string;
    subcategoryStatus:string;
    categoryId : number;
}

export class brandMaster{
    brandId:number;
    brandName:string;
    brandStatus:string;
}

export class unitMaster{
    unitId:number;
    unitName:string;
    looseUnit:boolean=false;
    unitStatus:string;
}

export class productMaster{
    productId:number;
    productName:string;
    productStatus:string;
    productDescription:string;
    mrp:number;
    retailDiscountPerc:number;
    retailDiscount:number;
    wholesaleDiscountPerc:number;
    wholesaleDiscount:number;
   // productPrice:string;
    scId: number;
    brandId:number;
    unitId:number;
   categoryId: number;
   retailPrice:number;
   wholesalePrice:number;
   unit:string;
   purchasePrice:number;
   looseUnit:boolean=false;
   currentStock:number;
   openingStock:number;
   barCode:string;
   
}

export class employeeMaster{
    employeeId:number;
    fullName:string;
    mobileNumber:string;
    empMailAddress:string;
    empPassword:string;
    employeeDOB:string;
    employeeRegisterDate:Date;
    empAddress:string;
    empDesignation:string;
    empPhoto:string;
    status:string;
    privilege:boolean=false;
    empId:string;

    //For priviledge
    brand:boolean=false;
    category:boolean=false;
    subcategory:boolean=false;
    product:boolean=false;
    unit:boolean=false;
    employee:boolean=false;
    currentstock:boolean=false;
    saleform:boolean=false;
    stock:boolean=false;
    stockhistory:boolean=false;
    invoicenowisedetails:boolean=false;
    adminsetting:boolean=false;
    changepwd:boolean=false;

}

export class forgotMaster{
    
    employeeId:number;
    otp:number;
    empPassword:string;
    newPassword : string;
    mobileNumber:string;
}

export class loginMaster{
    mobileNumber:string;
    empPassword:string;
}

export class saleMaster{
    saleId:number;
    barCode:string;
    invoiceDate:string;
    saleType:string;
    cashier:string;
    invoiceNo:string;
    payType:string;
    custMobileNumber:string;
    customerName:string;
    productName:string;
    looseUnit:boolean=false;
    unitDropDown:string;

    employeeId:number;

    totalItems:number=0;
    totalQuantity:number=0;
    totalMrp:number=0;
    totalRate:number=0;
    totalFinalAmt:number=0;
    totalPurchasePrice:number=0;
    youSave:number=0;

        productList : Array<prodMaster>=[];
    productList1 : Array<prodMaster>=[];
  //  orderDetailReqDtoList: Array<prodMaster>=[];
    // wholesaleList:any;
}

export class prodMaster{
    productId:number;
    unit:string;
    qty:number=0;
    mrp:number=0;
    rate:number=0;
    finalAmount:number=0;
    purchasePrice:number=0;
    productDescription:string;
    discountPerc:number=0;
    discount:number=0;
    currentStock:number;
    productName:string;
    unitName:string;
    looseUnit:boolean;
    unitDropDown:string;
}

export class changePswMaster{
    employeeId:number;
    empPassword:string;
    newPassword:string;
}

export class stockMaster{
    stockId:number;
    stock:number;
    addStockDate:string;
    // status:string;
    productId:number;
    orderId:number;
}

export class stockHistoryMaster{
    recordDate:string;
}

export class invoiceNoWiseDetailsMaster{
    saleId:number;
    invoiceDate:string;
    saleType:string;
    cashier:string;
    invoiceNo:string;
    payType:string;
    custMobileNumber:string;
    customerName:string;
    productName:string;
    looseUnit:boolean=false;
    unitDropDown:string;

    employeeId:number;

    totalItems:number=0;
    totalQuantity:number=0;
    totalMrp:number=0;
    totalRate:number=0;
    totalFinalAmt:number=0;
    youSave:number=0;

    productList : Array<prodMaster>=[];
  //  orderDetailReqDtoList: Array<prodMaster>=[];
    // wholesaleList:any;
    purchasePrice:number=0;
    totalPurchasePrice:number=0;
}

export class adminsetting{
    adminId:number;
    lowStock:number;
    
    }
export class nameexistmaster
{
   categoryName:string;
  productName:string;
  subcategoryName:string;
  unitName:string;
  brandName:string;
}