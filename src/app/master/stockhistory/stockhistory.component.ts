import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { stockHistoryMaster } from '../masterModels';
import { ToastrManager } from 'ng6-toastr-notifications';
import { HttpmethodsService } from 'src/app/service/httpmethods.service';
import { formatDate } from '@angular/common';
import { getStockHistoryRecords, getYesterdaysStockHistory } from '../mastersUrls';
import * as xlsx from 'xlsx';

@Component({
selector: 'app-stockhistory',
templateUrl: './stockhistory.component.html',
styleUrls: ['./stockhistory.component.css']
})
export class StockhistoryComponent implements OnInit {
@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
@ViewChild(MatSort, {static: true}) sort: MatSort;
@ViewChild('epltable', { static: false }) epltable: ElementRef;

stockHistoryForm: FormGroup;
stockHistoryModel= new stockHistoryMaster();
    maxdate: string;




constructor(private formBuilder: FormBuilder,public toastr: ToastrManager, private httpService: HttpmethodsService) { }
displayedColumns: string[] = ['no','categoryname','brandname','opening','closing','current'];

dataSource = new MatTableDataSource<stockHistoryMaster>();


ngOnInit() {
this.stockHistoryForm = this.formBuilder.group({
// stockId:new FormControl(''),
recordDate:new FormControl('', [Validators.required]),




})
this.maxdate = formatDate(new Date, 'yyyy-MM-dd', 'en');

this.getYesterdaysHistory();


this.dataSource.sort = this.sort;
// this.dataSource.paginator = this.paginator;
this.stockHistoryModel.recordDate = formatDate(new Date(), 'yyyy-MM-dd', 'en');
this.getStockHistoryRecords();


}

getStockHistoryRecords(){
// alert("getstock history")
this.httpService.postRequest(getStockHistoryRecords,this.stockHistoryModel).subscribe((data: any) => {
// console.log(data);
this.dataSource = new MatTableDataSource(data);
this.dataSource.paginator = this.paginator;
})
}

getYesterdaysHistory() {
this.httpService.getRequest(getYesterdaysStockHistory).subscribe((data: any) => {
// console.log(data);
//alert(JSON.stringify(data));    
this.dataSource = new MatTableDataSource(data);
// alert("yesterdays -> "+JSON.stringify(data));
this.dataSource.paginator = this.paginator;


})
}

exportToExcel() {
const ws: xlsx.WorkSheet =
xlsx.utils.table_to_sheet(this.epltable.nativeElement);
const wb: xlsx.WorkBook = xlsx.utils.book_new();
xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
xlsx.writeFile(wb, 'Stock History Reports.xlsx');
}

applyFilter(event){
const filterValue = (event.target as HTMLInputElement).value;
this.dataSource.filter = filterValue.trim().toLowerCase();

if (this.dataSource.paginator) {
this.dataSource.paginator.firstPage();
}
}



}