import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ToastrManager } from 'ng6-toastr-notifications';
import { HttpmethodsService } from 'src/app/service/httpmethods.service';
import { stockMaster } from '../masterModels';
import { createStock, getActiveProduct, getAllStock } from '../mastersUrls';
import { formatDate } from '@angular/common';
import * as xlsx from 'xlsx';

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.css']
})
export class StockComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild('epltable', { static: false }) epltable: ElementRef;

  stockForm: FormGroup;
  stockModel= new stockMaster();
  isEdit: boolean = true;
  productActiveList: any[];

 // totalQty:number=0;
  //quantityList=new Array<prodMaster>();
 


  constructor(private formBuilder: FormBuilder,public toastr: ToastrManager, private httpService: HttpmethodsService) { }
  //displayedColumns: string[] = ['no', 'countryname', 'statename', 'status','Action'];
  displayedColumns: string[] = ['no','categoryname','subcategoryname','brandname'];

  dataSource = new MatTableDataSource<stockMaster>();


  

  ngOnInit() {
    this.stockForm = this.formBuilder.group({
      stockId:new FormControl(''),
      stock:new FormControl('', [Validators.required]),
      addStockDate:new FormControl('', [Validators.required]),
     // status:new FormControl('', [Validators.required]),
      productId:new FormControl('', [Validators.required]),
      orderId:new FormControl(''),

    
    
    })


    this.dataSource.sort = this.sort;
    // this.dataSource.paginator = this.paginator;
    this.getActiveProduct();
    this.getAllStock();
    this.stockModel.addStockDate = formatDate(new Date(), 'yyyy-MM-dd', 'en');

  
  }

  getActiveProduct(){
    this.httpService.getRequest(getActiveProduct).subscribe((data: any) => {
     // console.log(data);
      this.productActiveList = data;
      })
  }

  addStockDetails(){
    if (this.isEdit) {
    this.httpService.postRequest(createStock, this.stockModel).subscribe((data: any) => {
      if(data){
      this.toastr.successToastr("Save Successfully...",'Success!');
      this.isEdit = true;
     // this.getAllCategory();
    //  setTimeout(() => this.formGroupDirective.resetForm(), 200);
    this.getAllStock();
      this.stockForm.reset(); 
      this.stockForm.markAsPristine();
      }
      else{
        this.toastr.errorToastr("Save Faield...",'Error!');
        //this.getAllCategory();
       this.getAllStock();
        this.isEdit = true;
      }
    })
  }
   }

  applyFilter(event){
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getAllStock() {
    this.httpService.getRequest(getAllStock).subscribe((data: any) => {
     // console.log(data);
      this.dataSource = new MatTableDataSource(data);
     // this.dataSource.paginator = this.paginator;
     // alert(JSON.stringify(this.dataSource));
    })
  }

  exportToExcel() {
    const ws: xlsx.WorkSheet =
    xlsx.utils.table_to_sheet(this.epltable.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'Stock Reports.xlsx');
    }
}
