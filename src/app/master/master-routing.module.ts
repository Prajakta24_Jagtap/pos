import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrandComponent } from './brand/brand.component';
import { CategoryComponent } from './category/category.component';
import { SubcategoryComponent } from './subcategory/subcategory.component';
import { UnitComponent } from './unit/unit.component';
import { ProductComponent } from './product/product.component';
import { EmployeeComponent } from './employee/employee.component';
import { ChangepwdComponent } from './changepwd/changepwd.component';
import { SaleformComponent } from './saleform/saleform.component';
import { StockComponent } from './stock/stock.component';
import { StockhistoryComponent } from './stockhistory/stockhistory.component';
import { InvoicenowisedetailsComponent } from './invoicenowisedetails/invoicenowisedetails.component';
import { AdminsettingComponent } from './adminsetting/adminsetting.component';
import { CurrentstockComponent } from './currentstock/currentstock.component';


const routes: Routes = [
  {path:'category',component:CategoryComponent},
  {path:'subcategory',component:SubcategoryComponent},
  {path:'unit',component:UnitComponent},
  {path:'brand',component:BrandComponent},
  {path:'product',component:ProductComponent},
  {path:'employee',component:EmployeeComponent},
  {path:'changepwd',component:ChangepwdComponent},
  {path:'saleform',component:SaleformComponent},
  {path:'stock',component:StockComponent},
  {path:'stockhistory',component:StockhistoryComponent},
  {path:'invoicenowisedetails',component:InvoicenowisedetailsComponent},
  {path:'adminsetting',component:AdminsettingComponent},
  {path:'currentstock',component:CurrentstockComponent},

  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MasterRoutingModule { }
