import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';

import { HttpmethodsService } from 'src/app/service/httpmethods.service';
import { cashierReportMaster } from '../reportModels';
import { getFilterByfromdatetodate, getfullName, getlistbyinvoiceDate } from '../reportUrls';
import { formatDate } from '@angular/common';
import * as xlsx from 'xlsx';

@Component({
selector: 'app-cashierreport',
templateUrl: './cashierreport.component.html',
styleUrls: ['./cashierreport.component.css']
})
export class CashierreportComponent implements OnInit {

cashierReportForm: FormGroup;
cashierReport=new cashierReportMaster();
fullNameList: any;




constructor(private formBuilder: FormBuilder, public toastr: ToastrManager, private router: Router, private httpService: HttpmethodsService,) { }
displayedColumns: string[] = ['no', 'cashier','invoiceDate','payType','saleType','invoiceNo','totalQuantity','totalMrp','totalRate','totalFinalAmt'];
dataSource = new MatTableDataSource<cashierReportMaster>();
@ViewChild(MatSort, { static: true }) sort: MatSort;
@ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
@ViewChild('epltable', { static: false }) epltable: ElementRef;


ngOnInit() {
this.cashierReportForm = this.formBuilder.group({
fromDate:new FormControl('',[Validators.required]),
toDate: new FormControl('',[Validators.required]),
payType:new FormControl(''),
cashier:new FormControl(''),
fullName:new FormControl('')

})

this.getfullNameList();
this.cashierReport.fromDate = formatDate(new Date(), 'dd-MM-yyyy', 'en');
this.cashierReport.invoiceDate = formatDate(new Date(),'dd-MM-yyyy', 'en');

// console.log("dateee"+this.cashierReport.invoiceDate);


//console.log("dateee JSon"+JSON.stringify(this.cashierReport.invoiceDate));
this.getListByCurrentdate();
}



getListByCurrentdate()
{
{
this.httpService.getRequest(getlistbyinvoiceDate).subscribe((data: any) => {
// console.log(data);
// this.saleModel = data;

this.dataSource = new MatTableDataSource(data);
this.dataSource.paginator = this.paginator;
// this.saleModel.orderDetailReqDtoList=this.retailList;
//alert(JSON.stringify( this.dataSource));
// console.log("jdjfhd"+JSON.stringify( this.dataSource));
})
}


// this.httpService.getRequest(getCategoryById + "/" + element.categoryId).subscribe((data: any) => {
// console.log(data);
// this.categoryModel = data;
// this.isEdit = false;

// })

}
getFilterByfromdatetodate()
{

//alert(" "+this.cashierReport.fromDate+" "+this.cashierReport.toDate+" "+this.cashierReport.payType+" "+this.cashierReport.employeeId);
this.httpService.postRequest(getFilterByfromdatetodate,this.cashierReport).subscribe((data: any) => {
// console.log(data);
// this.saleModel = data;

this.dataSource = new MatTableDataSource(data);
this.dataSource.paginator = this.paginator;
// this.saleModel.orderDetailReqDtoList=this.retailList;
//alert(JSON.stringify( this.dataSource));
// console.log("jdjfhd"+JSON.stringify( this.dataSource));
})
}

setEmployeeId(data)
{
this.cashierReport.employeeId=data.employeeId;
}

// // this.httpService.getRequest(getCategoryById + "/" + element.categoryId).subscribe((data: any) => {
// // console.log(data);
// // this.categoryModel = data;
// // this.isEdit = false;

// // })

// }

getfullNameList() {

// alert(this.cashierReport.fullName)

{
this.httpService.getRequest(getfullName).subscribe((data: any) => {
// console.log(data);
// this.saleModel = data;
this.fullNameList=data;
// this.saleModel.orderDetailReqDtoList=this.retailList;
//alert(JSON.stringify( this.fullNameList));
})
}


}

exportToExcel() {
    const ws: xlsx.WorkSheet =
    xlsx.utils.table_to_sheet(this.epltable.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'Cashierwise Reports.xlsx');
    }

    applyFilter(event){
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
    
        if (this.dataSource.paginator) {
          this.dataSource.paginator.firstPage();
        }
      }
}