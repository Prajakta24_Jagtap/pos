import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashierreportComponent } from './cashierreport.component';

describe('CashierreportComponent', () => {
  let component: CashierreportComponent;
  let fixture: ComponentFixture<CashierreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashierreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashierreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
