
export const getfullName ="/employee/active";

export const getlistbyinvoiceDate ="/order/getlistbyinvoiceDate";

export const getFilterByfromdatetodate ="/order/getByfromtodateandCashieName";

export const getDailyByfromtodateandCashieName ="/order/getDailyByfromtodateandCashieName";

export const getDailyOrderList ="/order/getDailyOrderList";


export const getProfitLoss="/order/getProfitLoss";

export const gettotalcnt ="/order/getReportOfTotal";
export const getStockAlert="/product/getStockAlert";


export const getlistbygroupbydate="/order/getlistbygroupbydate";

export const getDashboardTotalFinnalAmount="/order/getDashboardTotalFinnalAmount";