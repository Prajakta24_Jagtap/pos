import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { HttpmethodsService } from 'src/app/service/httpmethods.service';
import { profitLoss } from '../reportModels';
import { getProfitLoss } from '../reportUrls';

@Component({
  selector: 'app-monthlyprofitlosssreport',
  templateUrl: './monthlyprofitlosssreport.component.html',
  styleUrls: ['./monthlyprofitlosssreport.component.css']
})
export class MonthlyprofitlosssreportComponent implements OnInit {

 
  dailyProfitLossForm: FormGroup;
  obj=new profitLoss();
  maxdate: string;

  
constructor(private formBuilder: FormBuilder, public toastr: ToastrManager, private router: Router, private httpService: HttpmethodsService,) { }
// displayedColumns: string[] = ['no', 'cashier','invoiceDate','payType','saleType','invoiceNo','totalQuantity','totalMrp','totalRate','totalFinalAmt'];
// dataSource = new MatTableDataSource<cashierReportMaster>();
// @ViewChild(MatSort, { static: true }) sort: MatSort;
// @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
// @ViewChild('epltable', { static: false }) epltable: ElementRef;


ngOnInit() {
  

this.dailyProfitLossForm = this.formBuilder.group({
  monthDate:new FormControl('',[Validators.required]),


})
this.obj.monthDate=formatDate(new Date, 'yyyy-MM-dd', 'en');
this.getlist();

this.maxdate=formatDate(new Date, 'dd-MM-yyyy', 'en');
}



getlist()
{
this.httpService.postRequest(getProfitLoss, this.obj).subscribe((data: any) => {
// console.log(data);

if(data.profitcount!=null)
{

//  alert("Profit --- "+data);
  this.obj.name="Profit";
  this.obj.count=Math.round(data.profitcount);
}
else
{
 // alert(" Loss--- "+data);
  this.obj.name="Loss";
  this.obj.count=data.lossCount;
}



})
}

}