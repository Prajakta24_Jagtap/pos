import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthlyprofitlosssreportComponent } from './monthlyprofitlosssreport.component';

describe('MonthlyprofitlosssreportComponent', () => {
  let component: MonthlyprofitlosssreportComponent;
  let fixture: ComponentFixture<MonthlyprofitlosssreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonthlyprofitlosssreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthlyprofitlosssreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
