import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { getActiveBrand, getActiveCategory, getActiveSubCategorycatwise, getByBrandId } from 'src/app/master/mastersUrls';
import { HttpmethodsService } from 'src/app/service/httpmethods.service';
import { reporttotal } from '../reportModels';
import { gettotalcnt } from '../reportUrls';

@Component({
selector: 'app-totalamountreport',
templateUrl: './totalamountreport.component.html',
styleUrls: ['./totalamountreport.component.css']
})
export class TotalamountreportComponent implements OnInit {

totalamountReportForm: FormGroup;
reporttotal=new reporttotal();
fullNameList: any;
brandNameList: any;
subCategoryActiveList: any;
categoryActiveList: any;
listdata:Array<reporttotal>=[];
productList: any;


constructor(private formBuilder: FormBuilder, public toastr: ToastrManager, private router: Router, private httpService: HttpmethodsService,) { }
displayedColumns: string[] = ['no','TotalofMRP','TotalofRetail','TotalofWholeSale','TotalPurchase'];
dataSource = new MatTableDataSource<reporttotal>();
@ViewChild(MatSort, { static: true }) sort: MatSort;
@ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
ngOnInit()
{
this.totalamountReportForm = this.formBuilder.group({
categoryName:new FormControl(''),
subcategoryName:new FormControl(''),
brandName:new FormControl(''),
categoryId:new FormControl(''),
brandId:new FormControl(''),
scId:new FormControl(''),
productId:new FormControl(''),



})
this.getActiveCategory();
// this.getActiveSubCategory();
this.getActiveBrand();
// this.getActiveProduct();

this.getlist();
}

getActiveCategory(){
this.httpService.getRequest(getActiveCategory).subscribe((data: any) => {
// console.log(data);
this.categoryActiveList = data;
})
}

getActiveSubCategory(category){
// this.httpService.getRequest(getActiveSubCategorycatwise).subscribe((data: any) => {
// console.log(data);
// this.subCategoryActiveList = data;
// })

// alert("cat id"+JSON.stringify(category.categoryId))

this.httpService.getRequest(getActiveSubCategorycatwise + "/" + category.categoryId).subscribe((data: any) => {
// console.log(data);
this.subCategoryActiveList = data;


})

}

getActiveBrand(){
this.httpService.getRequest(getActiveBrand).subscribe((data: any) => {
// console.log(data);
this.brandNameList = data;
})
}

getActiveProduct(brand){
// this.httpService.getRequest(getActiveProduct).subscribe((data: any) => {
// console.log(data);
// this.productList = data;
// })
// alert("cat id"+JSON.stringify(brand.brandId))

this.httpService.getRequest(getByBrandId + "/" + brand.brandId).subscribe((data: any) => {
// console.log(data);
this.productList = data;


})
}

setName(data1){
this.reporttotal.categoryId=data1.categoryId;
// alert("customer name="+JSON.stringify( this.saleModel.customerName))
}

setName1(data1){
this.reporttotal.scId=data1.scId;
// alert("customer name="+JSON.stringify( this.saleModel.customerName))
}

setName2(data1){
this.reporttotal.brandId=data1.brandId;
// alert("customer name="+JSON.stringify( this.saleModel.customerName))
}


getlist()
{
this.httpService.postRequest(gettotalcnt, this.reporttotal).subscribe((data: any) => {
console.log(data);
// alert("data "+JSON.stringify(data));
this.reporttotal.totalofWholeSale= Math.round(data.totalofWholeSale);
this.reporttotal.totalofMRP= Math.round(data.totalofMRP);
this.reporttotal.totalofRetail= Math.round(data.totalofRetail);
this.reporttotal.totalPurchase=Math.round(data.totalPurchase);
// this.listdata.push(this.reporttotal);
// alert("dhgdhf"+JSON.stringify( this.reporttotal.totalofWholeSale));
this.listdata.push(this.reporttotal);
this.dataSource.data=this.listdata;
// alert("dhgdhf"+JSON.stringify( this.reporttotal.totalofWholeSale));
// alert("dhgdhf"+JSON.stringify(this.dataSource.data));


})
}

applyFilter(event){
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  
}