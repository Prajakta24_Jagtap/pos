import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalamountreportComponent } from './totalamountreport.component';

describe('TotalamountreportComponent', () => {
  let component: TotalamountreportComponent;
  let fixture: ComponentFixture<TotalamountreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalamountreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalamountreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
