import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { HttpmethodsService } from 'src/app/service/httpmethods.service';
import { profitLoss } from '../reportModels';
import { getProfitLoss } from '../reportUrls';

@Component({
  selector: 'app-weeklyprofitlosssreport',
  templateUrl: './weeklyprofitlosssreport.component.html',
  styleUrls: ['./weeklyprofitlosssreport.component.css']
})
export class WeeklyprofitlosssreportComponent implements OnInit {

  dailyProfitLossForm: FormGroup;
  obj=new profitLoss();
  maxdate: string;


  constructor(private formBuilder: FormBuilder, public toastr: ToastrManager, private router: Router, private httpService: HttpmethodsService,) { }


  ngOnInit() {

    
this.dailyProfitLossForm = this.formBuilder.group({
  weekDate:new FormControl('',[Validators.required]),


})
this.obj.weekDate=formatDate(new Date, 'yyyy-MM-dd', 'en');
this.getlist();

this.maxdate=formatDate(new Date, 'dd-MM-yyyy', 'en');
  }


  

getlist()
{
this.httpService.postRequest(getProfitLoss, this.obj).subscribe((data: any) => {
// console.log(data);

if(data.profitcount!=null)
{

  alert("Profit --- "+data);
  this.obj.name="Profit";
  this.obj.count=Math.round(data.profitcount);
}

if(data.lossCount!=null)
{
 alert(" Loss--- "+data);
  this.obj.name="Loss";
  this.obj.count=Math.round(data.lossCount);
}

})

}
}