import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeeklyprofitlosssreportComponent } from './weeklyprofitlosssreport.component';

describe('WeeklyprofitlosssreportComponent', () => {
  let component: WeeklyprofitlosssreportComponent;
  let fixture: ComponentFixture<WeeklyprofitlosssreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeeklyprofitlosssreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeeklyprofitlosssreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
