import { ElementRef, ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { productMaster } from 'src/app/master/masterModels';
import { HttpmethodsService } from 'src/app/service/httpmethods.service';
import { getStockAlert } from '../reportUrls';
import * as xlsx from 'xlsx';

@Component({
selector: 'app-notifyminstock',
templateUrl: './notifyminstock.component.html',
styleUrls: ['./notifyminstock.component.css']
})
export class NotifyminstockComponent implements OnInit {

categoryForm1: FormGroup;
lowStock: number;
constructor(private formBuilder: FormBuilder, public toastr: ToastrManager, private router: Router, private httpService: HttpmethodsService,) { }
displayedColumns: string[] = ['no', 'productName','currentStock'];
dataSource = new MatTableDataSource<productMaster>();
@ViewChild(MatSort, { static: true }) sort: MatSort;
@ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
@ViewChild('epltable', { static: false }) epltable: ElementRef;


ngOnInit() {
this.categoryForm1 = this.formBuilder.group({
// categoryName: new FormControl('', [Validators.required, Validators.pattern("[a-zA-Z\g]* $"),]),


})

this.getStockAlert();
}

getStockAlert() {
// this.httpService.getRequest(getStockAlert).subscribe((data: any) => {
// console.log(data);
// // alert(JSON.stringify(data));
// this.dataSource = new MatTableDataSource(data);
// this.dataSource.paginator = this.paginator;
// //ng serve alert(JSON.stringify(this.dataSource));
// })


this.httpService.getRequest(getStockAlert).subscribe((data: any) => {
// console.log(data);
// console.log(data);
// console.log("length:"+JSON.stringify(data.length))
// alert(JSON.stringify(data));
this.dataSource = new MatTableDataSource(data);
this.dataSource.paginator = this.paginator;
//ng serve alert(JSON.stringify(this.dataSource));


})



}

exportToExcel() {
    const ws: xlsx.WorkSheet =
    xlsx.utils.table_to_sheet(this.epltable.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'Low Stock Reports.xlsx');
    }

    applyFilter(event){
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
    
        if (this.dataSource.paginator) {
          this.dataSource.paginator.firstPage();
        }
      }
}