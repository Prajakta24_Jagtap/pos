import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotifyminstockComponent } from './notifyminstock.component';

describe('NotifyminstockComponent', () => {
  let component: NotifyminstockComponent;
  let fixture: ComponentFixture<NotifyminstockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotifyminstockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotifyminstockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
