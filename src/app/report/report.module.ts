import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatPaginatorModule, MatFormFieldModule, MatInputModule, MatCardModule, MatDatepickerModule, MatSelectModule, MatAutocompleteModule, MatSlideToggleModule, MatProgressSpinnerModule, MatNativeDateModule, MatRadioModule, MatCheckboxModule, MatSidenavModule, MatDividerModule, MatDialogModule, MatButtonModule, MatIconModule, MatTableModule, MatSortModule } from '@angular/material';
import { SharedModule } from '../shared/shared.module';
import { ReportRoutingModule } from './report-routing.module';
import { CashierreportComponent } from './cashierreport/cashierreport.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DailyreportComponent } from './dailyreport/dailyreport.component';
import { DailtreportdatewiseComponent } from './dailtreportdatewise/dailtreportdatewise.component';
import { TotalamountreportComponent } from './totalamountreport/totalamountreport.component';
import { NotifyminstockComponent } from './notifyminstock/notifyminstock.component';
import { DailyprofitlossreportComponent } from './dailyprofitlossreport/dailyprofitlossreport.component';
import { WeeklyprofitlosssreportComponent } from './weeklyprofitlosssreport/weeklyprofitlosssreport.component';
import { MonthlyprofitlosssreportComponent } from './monthlyprofitlosssreport/monthlyprofitlosssreport.component';
import { YearlyprofitlosssreportComponent } from './yearlyprofitlosssreport/yearlyprofitlosssreport.component';


@NgModule({
  declarations: [CashierreportComponent, DailyreportComponent, DailtreportdatewiseComponent, TotalamountreportComponent, NotifyminstockComponent, DailyprofitlossreportComponent, WeeklyprofitlosssreportComponent, MonthlyprofitlosssreportComponent, YearlyprofitlosssreportComponent],
  imports: [
    CommonModule,
    ReportRoutingModule,
    MatPaginatorModule,
    SharedModule, 
    MatFormFieldModule,
    MatInputModule,
    MatCardModule, 
    MatDatepickerModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatSlideToggleModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatDividerModule,
    MatDialogModule,
    MatButtonModule,
    MatButtonModule,
    MatDialogModule, 
    MatIconModule,
    MatSidenavModule,
    MatDividerModule,
    MatDialogModule,
    MatButtonModule,
    MatSelectModule,
    MatTableModule,
    MatSortModule
  ]
})
export class ReportModule { }
