import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyprofitlossreportComponent } from './dailyprofitlossreport.component';

describe('DailyprofitlossreportComponent', () => {
  let component: DailyprofitlossreportComponent;
  let fixture: ComponentFixture<DailyprofitlossreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyprofitlossreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyprofitlossreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
