import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YearlyprofitlosssreportComponent } from './yearlyprofitlosssreport.component';

describe('YearlyprofitlosssreportComponent', () => {
  let component: YearlyprofitlosssreportComponent;
  let fixture: ComponentFixture<YearlyprofitlosssreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YearlyprofitlosssreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YearlyprofitlosssreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
