import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CashierreportComponent } from './cashierreport/cashierreport.component';
import { DailtreportdatewiseComponent } from './dailtreportdatewise/dailtreportdatewise.component';
import { DailyprofitlossreportComponent } from './dailyprofitlossreport/dailyprofitlossreport.component';
import { DailyreportComponent } from './dailyreport/dailyreport.component';
import { MonthlyprofitlosssreportComponent } from './monthlyprofitlosssreport/monthlyprofitlosssreport.component';
import { NotifyminstockComponent } from './notifyminstock/notifyminstock.component';
import { TotalamountreportComponent } from './totalamountreport/totalamountreport.component';
import { WeeklyprofitlosssreportComponent } from './weeklyprofitlosssreport/weeklyprofitlosssreport.component';
import { YearlyprofitlosssreportComponent } from './yearlyprofitlosssreport/yearlyprofitlosssreport.component';


const routes: Routes = [
  {path:'cashierreport',component:CashierreportComponent},
  {path:'dailyreport',component:DailyreportComponent},
  {path:'dailtreportdatewise',component:DailtreportdatewiseComponent},
  {path:'totalamountreport',component:TotalamountreportComponent},
  {path:'notifyminstock',component:NotifyminstockComponent},
  {path:'dailyprofitlossreport',component:DailyprofitlossreportComponent},
  {path:'monthlyprofitlosssreport',component:MonthlyprofitlosssreportComponent},
  {path:'weeklyprofitlosssreport',component:WeeklyprofitlosssreportComponent},
  {path:'yearlyprofitlosssreport',component:YearlyprofitlosssreportComponent},
  
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportRoutingModule { }
