import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailtreportdatewiseComponent } from './dailtreportdatewise.component';

describe('DailtreportdatewiseComponent', () => {
  let component: DailtreportdatewiseComponent;
  let fixture: ComponentFixture<DailtreportdatewiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailtreportdatewiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailtreportdatewiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
