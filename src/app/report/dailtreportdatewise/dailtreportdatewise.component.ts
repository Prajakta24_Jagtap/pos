import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { HttpmethodsService } from 'src/app/service/httpmethods.service';
import { cashierReportMaster } from '../reportModels';
import { getDailyOrderList, getfullName, getlistbyinvoiceDate } from '../reportUrls';
import * as xlsx from 'xlsx';

@Component({
selector: 'app-dailtreportdatewise',
templateUrl: './dailtreportdatewise.component.html',
styleUrls: ['./dailtreportdatewise.component.css']
})
export class DailtreportdatewiseComponent implements OnInit {

dailycashierReportForm: FormGroup;
cashierReport=new cashierReportMaster();
fullNameList: any;


constructor(private formBuilder: FormBuilder, public toastr: ToastrManager, private router: Router, private httpService: HttpmethodsService,) { }
displayedColumns: string[] = ['no', 'cashier','invoiceDate','payType','saleType','invoiceNo','totalQuantity','totalMrp','totalRate','totalFinalAmt','Action'];
dataSource = new MatTableDataSource<cashierReportMaster>();
@ViewChild(MatSort, { static: true }) sort: MatSort;
@ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
@ViewChild('epltable', { static: false }) epltable: ElementRef;

ngOnInit() {

this.dailycashierReportForm = this.formBuilder.group({
fromDate:new FormControl('',[Validators.required]),
payType:new FormControl(''),
cashier:new FormControl(''),
fullName:new FormControl('')


})

this.getfullNameList();
this.getListByCurrentdate();
}



getfullNameList() {


this.httpService.getRequest(getfullName).subscribe((data: any) => {
// console.log(data);
// this.saleModel = data;
this.fullNameList=data;
// this.saleModel.orderDetailReqDtoList=this.retailList;
//alert(JSON.stringify( this.fullNameList));
})

}


getListByCurrentdate()
{

this.httpService.getRequest(getlistbyinvoiceDate).subscribe((data: any) => {
// console.log(data);
// this.saleModel = data;

this.dataSource = new MatTableDataSource(data);
//this.dataSource.paginator = this.paginator;
// this.saleModel.orderDetailReqDtoList=this.retailList;
//alert(JSON.stringify( this.dataSource));
// console.log("jdjfhd"+JSON.stringify( this.dataSource));
})



}


getFilterByfromdatetodate()
{

//alert(" "+this.cashierReport.fromDate+" "+this.cashierReport.payType+" "+this.cashierReport.employeeId);
this.httpService.postRequest(getDailyOrderList,this.cashierReport).subscribe((data: any) => {
// console.log(data);
// this.saleModel = data;

this.dataSource = new MatTableDataSource(data);
this.dataSource.paginator = this.paginator;
// this.saleModel.orderDetailReqDtoList=this.retailList;
//alert(JSON.stringify( this.dataSource));
// console.log("jdjfhd"+JSON.stringify( this.dataSource));
})
}


setEmployeeId(data)
{
this.cashierReport.employeeId=data.employeeId;
}

sendInvoiceNo(invoiceNo){

   // alert("invoiceNo="+JSON.stringify(invoiceNo))
    sessionStorage.setItem("invoiceNo",invoiceNo);
    sessionStorage.setItem("isLoad","true")
    this.router.navigateByUrl("/layout/invoicenowisedetails")

}

exportToExcel() {
    const ws: xlsx.WorkSheet =
    xlsx.utils.table_to_sheet(this.epltable.nativeElement);
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    xlsx.writeFile(wb, 'Cashier Datewise Daily Reports.xlsx');
    }

    applyFilter(event){
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
    
        if (this.dataSource.paginator) {
          this.dataSource.paginator.firstPage();
        }
      }
}